#!/bin/bash

function get_protoc() {
  local download_url="$1"
  local prefix="$2"
  shift 2
  local local_archive="protoc.zip"
  local tmpdir="$(mktemp -d)"
  if [[ -z "$tmpdir" || ! -d "$tmpdir" ]] ; then
    echo "bad tmpdir '$tmpdir'" >/dev/stderr
    return 1
  fi
  pushd "$tmpdir"

  curl -L "$download_url" -o "$local_archive"
  unzip "$local_archive"
  install -d -m 700 "$prefix"
  install -m 700 "./bin/protoc" "$prefix/bin/protoc"
  install -d -m 700 "$prefix/include/google/protobuf"
  cp -R "./include/google/protobuf/" "$prefix/include/google/protobuf"

  rm -rf "$tmpdir"
}

function main() {
  local base="https://github.com/protocolbuffers/protobuf/releases/download"

  case "$(uname -o -m)" in
    "Darwin arm64")
      get_protoc "$base/v26.1/protoc-26.1-osx-aarch_64.zip" "$HOME/.opt"
      ;;
    *)
      ;;
  esac
}

main "$@"

# See configuration.nix(5) or the NixOS manual by running nixos-help.

{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    # grub = {
    #   enable = true;
    #   efiSupport = true;
    #   useOSProber = true;
    #   device = "nodev";
    # };
  };
  networking.hostName = "HOSTNAME";
  networking.networkmanager.enable = true;
  networking.wireguard.enable = true;
  # Firewall:
  networking.firewall.allowedTCPPorts = [ 8000 8001 8002 8003 8004 8005 ];
  # networking.firewall.allowedUDPPorts = [...];
  # networking.firewall.enable = false; # Disable firewall.

  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.driSupport = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
    support32Bit = true;
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
  };
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";

  time.timeZone = "America/Los_Angeles";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    alsaUtils
    bash
    bazel
    bind
    clang
    clang-tools
    clang-manpages
    curl
    dict
    dunst
    (emacs.override {withGTK2 = false; withGTK3 = false;})
    file
    firefox
    gcc
    gdb
    git
    gnome3.adwaita-icon-theme
    go
    google-chrome
    google-cloud-sdk
    google-compute-engine
    google-fonts
    grpc
    i3
    imagemagick
    inkscape
    ispell
    iw
    jq
    leveldb
    manpages # Also see https://nixos.wiki/wiki/Man_pages
    # notmuch
    # openjdk
    protobuf
    # python27Packages.virtualenv
    rofi
    rsync
    rustc
    sbcl
    screen
    texlive.combined.scheme-tetex
    tmux
    unzip
    vanilla-dmz
    vim
    whois
    xterm
    zip
    zsh
  ];

  fonts = {
    enableDefaultFonts = true;
    fonts = [
      pkgs.fira-code
      pkgs.fira-code-symbols
      pkgs.merriweather
      pkgs.merriweather-sans
      pkgs.opensans-ttf
      pkgs.paratype-pt-mono
      pkgs.paratype-pt-sans
      pkgs.paratype-pt-serif
      pkgs.roboto
      pkgs.vazir-fonts
    ];
    fontconfig = {
      defaultFonts = {
        # This is how you can add multiple fonts.  Not sure if the order is
        # significant, and I haven't tested this.
        serif = ["Vazir" "Merriweather" ];
        sansSerif = ["Roboto"];
        monospace = ["FiraCode"];
      };
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    # Seems broken.  Turned off in favour of programs.ssh.startAgent.
    # enableSSHSupport = true;
  };
  programs.ssh.startAgent = true;
  programs.zsh.enable = true;

  systemd.user.services."dunst" = {
    enable = true;
    description = "Notification daemon";
    wantedBy = [ "default.target" ];
    serviceConfig.Restart = "always";
    serviceConfig.RestartSec = 2;
    serviceConfig.ExecStart = "${pkgs.dunst}/bin/dunst";
  };

  services.openssh.enable = true; # The OpenSSH daemon

  nixpkgs.config.allowUnfree = true;
  hardware.opengl = {
    driSupport = true;
    driSupport32Bit = true;
  };
  services.xserver = {
    enable = true;
    layout = "us";
    xkbOptions = "ctrl:nocaps,altwin:swap_alt_win";
    videoDrivers = ["nvidia"];
    displayManager.autoLogin = {
      enable = true;
      user = "DESIRED-USERNAME";
    };
    displayManager.sddm = {
      enable = true;
      enableHidpi = true;
    };
    windowManager.i3.enable = true;
    libinput.enable = true;
    libinput.mouse.naturalScrolling = true;
    displayManager.defaultSession = "none+i3";
  };

  services.printing = {
    enable = true;
    drivers = [ pkgs.brlaser pkgs.hll2390dw-cups ];
  };
  services.avahi.enable = true;
  services.upower.enable = true;

  sound.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.DESIRED-USERNAME = {
    description = "DESIRED-USERNAME-DESCRIPTION";
    isNormalUser = true;
    uid = 1000;
    extraGroups = ["networkmanager" "wheel" "sound" "audio" "video"];
    shell = pkgs.zsh;
  };

  services.udev = {
    extraRules = ''
      KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="1050", ATTRS{idProduct}=="0113|0114|0115|0116|0120|0200|0402|0403|0406|0407|0410", TAG+="uaccess"
      '';
  };

  system.stateVersion = "21.05";
}

# Cleanup gc roots:
# $ rm /nix/var/nix/gcroots/auto/*
# $ nix-collect-garbage -d

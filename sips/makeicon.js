// HACK! TODO: Get the name and dir from somewhere more appropriate.
const app_name = sips.arguments[2];
const output_dir = sips.outputDir;

const cr_image = sips.images.pop();

function drawIcon(size, fontSize, text, high) {
  const canvas = new Canvas(size, size);
  const mainLogoSize = Math.floor(size * 10 / 16);
  canvas.drawImage(cr_image, size - mainLogoSize, size - mainLogoSize,
                   mainLogoSize, mainLogoSize);

  const x = size / 32;
  const y = size / 32;

  // canvas.fillStyle = 'rgba(0,0,0, 0.6)';
  // canvas.fillRect(0, y, size, fontSize + y);

  canvas.fillStyle = '#fff';
  canvas.font = '' + fontSize + 'pt Merriweather-Bold';
  canvas.textAlign = 'center';
  canvas.shadowOffsetX = 0;
  canvas.shadowOffsetY = - fontSize/24;
  canvas.shadowBlur = fontSize/12;
  canvas.shadowColor = 'rgba(0,0,0, 0.3)';
  canvas.fillText(text, size / 2, fontSize + y);

  const filename = high ?
        ('icon_' + size/2 + 'x' + size/2 + '@2x.png') :
        ('icon_' + size + 'x' + size + '.png');
  const output = new Output(canvas, output_dir + '/' + filename);
  output.addToQueue();
}

drawIcon(16, 6, app_name[0], false);
drawIcon(32, 8, app_name[0], false);
drawIcon(32, 8, app_name[0], true); // 16x16@2x.
drawIcon(64, 12, app_name, false);
drawIcon(128, 24, app_name, false);
drawIcon(256, 48, app_name, false);
drawIcon(256, 48, app_name, true); // 128x128@2x
drawIcon(512, 96, app_name, false);
drawIcon(512, 96, app_name, true); // 256x256@2x

def deploy_local(name, srcs, dest_dir):
    def _deploy_impl(ctx):
        target_dir = _expand_vars_and_user(ctx.attr.dest_dir)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        src_files = [f.path for f in ctx.files.srcs]
        ctx.action(
            outputs = [ctx.actions.declare_file(target_dir + "/dummy")],
            inputs = ctx.files.srcs,
            executable = "/usr/bin/rsync",
            arguments = ["-av", "--files-from=-", "--from-file=-", "--", ctx.workspace_dir + "/", target_dir],
            input_content = "\n".join([os.path.basename(f) for f in src_files]) + "\n",
        )
        return []

    def _expand_vars_and_user(path):
        import os
        return os.path.expanduser(os.path.expandvars(path))


    native.rule(
        implementation = _deploy_impl,
        attrs = {
            "srcs": attr.label_list(allow_files = True),
            "dest_dir": attr.string(mandatory = True),
        },
    )(name = name, srcs = srcs, dest_dir = dest_dir)

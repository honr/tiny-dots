#pragma once
#include <stdint.h>

typedef int m_err;  // zero means OK.

// m_slice somewhat resembles slices in golang.

typedef struct {
  char* dat;    // Backing data
  int64_t len;  // Size
  int64_t cap;  // Capacity
} m_slice;

m_slice m_slice_make(int64_t cap);
// min_cap should be at least 4 (arbitrarily).
m_err m_slice_resize(m_slice* s, int64_t cap, int64_t min_cap);
m_err m_slice_close(m_slice* s);
m_slice m_slice_empty_str(int64_t cap);
m_err m_slice_write_str(m_slice*s, int64_t* cur, const char* str);

m_err m_error(m_err err, char* msg);
int64_t m_current_time_msec();


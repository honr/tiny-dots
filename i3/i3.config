# Please see http://i3wm.org/docs/userguide.html for reference.

set $mod Mod4
floating_modifier $mod
hide_edge_borders both
default_border normal 2
default_floating_border normal 2
workspace_layout stacking
title_align center
# For i3wm version 4.22 and above:
# gaps inner 4px
# gaps outer -7px
# smart_gaps inverse_outer

set $hub exec --no-startup-id hub

# Theme
# fg: foreground, bg: background.
# u: urgent, f: focused active, a: unfocused active, b: unfocused, i: inactive.
set_from_resource $color_bgu  i3wm.color_bgu
set_from_resource $color_fgu  i3wm.color_fgu
set_from_resource $color_bgf  i3wm.color_bgf
set_from_resource $color_fgf  i3wm.color_fgf
set_from_resource $color_bga  i3wm.color_bga
set_from_resource $color_fga  i3wm.color_fga
set_from_resource $color_bgb  i3wm.color_bgb
set_from_resource $color_fgb  i3wm.color_fgb
set_from_resource $color_bg1  i3wm.color_bg1
set_from_resource $color_fg1  i3wm.color_fg1
set_from_resource $color_bgi  i3wm.color_bgi
set_from_resource $color_fgi  i3wm.color_fgi
set_from_resource $color_test i3wm.color_test
set_from_resource $font       i3wm.font "pango:RobotoCondensed 11"

font $font

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
  position top
  # status_command hub i3status
  status_command i3status
  # tray_output primary
  separator_symbol "  "
  colors {
    #                 | border   |  bg      | text
    background         $color_bg1
    statusline         $color_fg1
    focused_workspace  $color_bgf $color_bgf $color_fgf
    active_workspace   $color_bga $color_bga $color_fga
    inactive_workspace $color_bgi $color_bgi $color_fgi
    urgent_workspace   $color_bgu $color_bgu $color_fgu
  }
}

# Class                | border |   bg  | text  | indicator | child_border
client.focused           $color_bgf  $color_bgf  $color_fgf  $color_test $color_bgf
client.focused_inactive  $color_bga  $color_bga  $color_fga  $color_test $color_bga
client.unfocused         $color_bgb  $color_bgb  $color_fgb  $color_test $color_bgb
client.urgent            $color_bgu  $color_bgu  $color_fgu  $color_test $color_bgu
client.placeholder       $color_test $color_test $color_test $color_test $color_test
client.background        $color_bg1

bindsym $mod+Return       $hub term
bindsym $mod+semicolon    $hub emacsclient
bindsym $mod+Shift+Return exec --no-startup-id alacritty
bindsym $mod+o            $hub browse
bindsym $mod+Shift+o      $hub browse-incognito
bindsym $mod+Shift+colon  exec --no-startup-id rofi -i -show drun -dpi 1
bindsym $mod+space        exec --no-startup-id rofi -i -show window -dpi 1

# set $top_pos border none, resize set width 100 ppt height 200, move position 0 0
# for_window [class="Scratch.Alacritty"] border normal 10, move position 0 0

# set $top_pos border none, resize set width 100 ppt height 200, move position 0 0
# for_window [class="Scratch.Alacritty"] {
#   move scratchpad
#   [class="Scratch.Alacritty"] scratchpad show
#   $top_pos
# }
# bindsym bindsym $mod+apostrophe exec 'i3msg "[class=Scratch.Alacritty]" scratchpad show || \
#     FLAVOUR=scratch alacritty --class "Scratch.Alacritty" --config-file=$HOME/.config/alacritty/scratch.yml'; $top_pos

# for_window [class="Scratch.Alacritty"] {
#   move scratchpad
#   $top_pos
# }

bindsym $mod+d mode "run"
mode "run" {
  bindsym Escape    mode "default"
  bindsym Control+g mode "default"

  bindsym x         exec --no-startup-id rofi -i -show drun -dpi 1; mode "default"

  bindsym Return    $hub term; mode "default"
  bindsym j         $hub term-screen; mode "default"
  bindsym $mod+t    exec --no-startup-id "$HOME/.local/bin/st"; mode "default"
  bindsym $mod+a    exec --no-startup-id alacritty; mode "default"
  bindsym $mod+x    exec --no-startup-id xterm; mode "default"
  bindsym $mod+u    exec --no-startup-id urxvt; mode "default"
  bindsyn $mod+v    $hub ee --eval "(progn (vterm t) (setq-local mode-line-format nil))"

  bindsym semicolon $hub ee; mode "default"
  bindsym e         $hub ee; mode "default"
  bindsym i         $hub ee "$HOME/.config/i3/config"; mode "default"
  bindsym $mod+i    $hub ee "$HOME/.Xresources"; mode "default"
  bindsym k         $hub ee '$QUESTDIR'; mode "default"
  bindsym s         $hub ee '$QUESTSDIR/$QUEST'; mode "default"

  bindsym g         $hub browse ; mode "default"
  bindsym o         $hub browse-incognito ; mode "default"
  bindsym $mod+g    $hub browser-pick; mode "default"
  bindsym $mod+o    $hub browser-pick incognito; mode "default"
  bindsym v         exec --no-startup-id vivaldi; mode "default"
  bindsym b         exec --no-startup-id vivaldi --incognito; mode "default"
  bindsym f         exec --no-startup-id firefox; mode "default"
  bindsym n         exec --no-startup-id firefox --private-window; mode "default"
  # This doesn't really work:
  # bindsym $mod+f    $run ls ~/.mozilla/firefox/ | $dmenu -p 'Firefox dir' | xargs -I@ firefox --profile="$HOME/.mozilla/firefox/@" --no-remote; mode "default"
}

bindsym $mod+q kill
# TODO
# bindsym $mod+Shift+q exec --no-startup-id i3-input -F '[workspace=^%s$] kill' -P 'kill workspace? '
# bindsym $mod+Shift+q $hub all-topics | $dmenu -p 'kill workspace? '

# change focus
bindsym $mod+b focus left
bindsym $mod+n focus down
bindsym $mod+p focus up
bindsym $mod+f focus right
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+u focus parent
bindsym $mod+i focus child

# move focused window
bindsym $mod+Shift+b move left
bindsym $mod+Shift+n move down
bindsym $mod+Shift+p move up
bindsym $mod+Shift+f move right
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

bindsym $mod+m           move scratchpad
bindsym $mod+Shift+m     scratchpad show
bindsym $mod+minus       scratchpad show

# switch workspaces
bindsym $mod+grave workspace back_and_forth
bindsym $mod+Escape workspace back_and_forth
bindsym $mod+Mod1+Left workspace prev
bindsym $mod+Mod1+Right workspace next
bindsym $mod+h workspace prev
bindsym $mod+l workspace next
#
bindsym $mod+1 workspace agenda
bindsym $mod+2 workspace box
bindsym $mod+3 workspace comm
bindsym $mod+4 workspace design
bindsym $mod+5 workspace em
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# switch and move workspace between outputs (displays)
bindsym $mod+Shift+bracketleft move workspace to output left
bindsym $mod+Shift+bracketright move workspace to output right
bindsym $mod+Shift+backslash move workspace to output next
bindsym $mod+bracketleft focus output left
bindsym $mod+bracketright focus output right
bindsym $mod+backslash focus output next

# move focused container to workspace
bindsym $mod+Shift+h move container to workspace prev
bindsym $mod+Shift+l move container to workspace next
bindsym $mod+Shift+1 move container to workspace agenda
bindsym $mod+Shift+2 move container to workspace box
bindsym $mod+Shift+3 move container to workspace comm
bindsym $mod+Shift+4 move container to workspace design
bindsym $mod+Shift+5 move container to workspace em
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

bindsym $mod+e mode "layout"
mode "layout" {
  bindsym Escape mode "default"
  bindsym Control+g mode "default"
  bindsym Return mode "default"

  bindsym f fullscreen toggle;   mode "default"
  bindsym e layout toggle stacking splitv tabbed; mode "default"
  bindsym s layout stacking;     mode "default"
  bindsym t layout tabbed;       mode "default"
  bindsym r layout splitv;       mode "default"
  bindsym w layout splith;       mode "default"

  bindsym v split vertical;      mode "default"
  bindsym z split horizontal;    mode "default"

  bindsym h resize shrink width 5 px or 5 ppt
  bindsym j resize grow height 5 px or 5 ppt
  bindsym k resize shrink height 5 px or 5 ppt
  bindsym l resize grow width 5 px or 5 ppt
  bindsym Left resize shrink width 5 px or 5 ppt
  bindsym Down resize grow height 5 px or 5 ppt
  bindsym Up resize shrink height 5 px or 5 ppt
  bindsym Right resize grow width 5 px or 5 ppt

  bindsym minus border none
  bindsym 0 border normal 0
  bindsym 1 border normal 1
  bindsym 2 border normal 2
  bindsym 3 border normal 4
  bindsym 4 border pixel 4

  bindsym apostrophe bar mode toggle
}

bindsym $mod+a         $hub switch-topic
bindsym $mod+Tab       $hub switch-topic-active
bindsym $mod+Shift+a   $hub move-topic
bindsym $mod+Control+a $hub rename-topic
# bindsym $mod+Escape  mode "exit"

# toggle tiling / floating
bindsym $mod+t floating toggle
# change focus between tiling / floating windows
bindsym $mod+Shift+t focus mode_toggle

bindsym $mod+s mode "settings"
mode "settings" {
  bindsym Escape    mode "default"
  bindsym Control+g mode "default"

  bindsym m       $hub audio mute-toggle
  bindsym a       $hub audio decrease
  bindsym s       $hub audio increase
  # bindsym XF86AudioMicMute $hub audio mute-mic-toggle
  # bindsym $mod+F4 $hub audio mute-mic-toggle
  bindsym b       $hub brightness decrease
  bindsym n       $hub brightness increase
  # bindsym XF86Display
  # bindsym XF86Messenger
  # bindsym XF86Go
  # bindsym Cancel
  # bindsym XF86Favorites
  bindsym t exec --no-startup-id $dmenu -i -p 'Theme ' | xargs -I@ $hub theme "@"
}

bindsym XF86AudioMute         $hub audio mute-toggle
bindsym $mod+F1               $hub audio mute-toggle
bindsym XF86AudioLowerVolume  $hub audio decrease
bindsym $mod+F2               $hub audio decrease
bindsym XF86AudioRaiseVolume  $hub audio increase
bindsym $mod+F3               $hub audio increase
# bindsym XF86AudioMicMute $hub audio mute-mic-toggle
# bindsym $mod+F4 $hub audio mute-mic-toggle
bindsym XF86MonBrightnessDown $hub brightness decrease
bindsym $mod+F5               $hub brightness decrease
bindsym XF86MonBrightnessUp   $hub brightness increase
bindsym $mod+F6               $hub brightness increase
# bindsym XF86Display
# bindsym XF86Messenger
# bindsym XF86Go
# bindsym Cancel
# bindsym XF86Favorites

# Screenshot a window (that you select).
bindsym --release $mod+Print exec "import ${HOME}/Desktop/screen-$(date -Iseconds).png"

bindsym $mod+Control+q exec --no-startup-id i3lock --color=$color_bg1
bindsym $mod+z mode "exit"
mode "exit" {
  bindsym Escape mode "default"
  bindsym Control+g mode "default"

  bindsym q exec --no-startup-id i3lock --color=$color_bg1 ; mode "default"
  bindsym --release a exec --no-startup-id Arc/zzz/locke/locke ; mode "default"
  bindsym --release s exec --no-startup-id Arc/zzz/locke/locke ; exec --no-startup-id systemctl suspend ; mode "default"
  bindsym --release x exec --no-startup-id .local/bin/x ; mode "default"
  bindsym l reload ; mode "default"
  # bindsym z exec --no-startup-id xsetroot -solid --color $color_bga ; mode "default"
  bindsym r restart ; mode "default"

  # Not working correctly:
  bindsym t \
          exec --no-startup-id rofi -dmenu -dpi 1 -i -p 'Theme' | \
          xargs -I@ .local/bin/set_theme "@" ; \
          mode "default"

  bindsym v \
          exec --no-startup-id xrdb -load ~/.Xresources ; \
          exec --no-startup-id ls ~/Pool/base/etc/Xresources-* | \
          rofi -dmenu -dpi 1 -i -p 'Xresources' | \
          xargs -I@ xrdb -merge "@" ; \
          mode "default"
  bindsym $mod+i exec --no-startup-id xrdb -load ~/.Xresources ; restart ; mode "default"
  # Exit i3 (logs you out of your X session)
  bindsym e exec --no-startup-id "i3-nagbar -t warning -m 'Exit the i3 session?' -b 'Yes, exit i3' 'i3-msg exit'"
  bindsym Shift+e exit; mode "default"
}

# bindswitch lid:on exec --no-startup-id Arc/zzz/locke/locke ; exec --no-startup-id systemctl suspend

bindsym --release --border button2 kill

for_window[workspace="^[.]"] floating enable
for_window[floating_from="user"] border normal 2

include $HOME/Pool/host/i3.d/*

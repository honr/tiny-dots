// Should go in ~/Library/KeyBindings/
{
  "^_" = "undo:";
  "^v" = "pageDown:";
  "~v" = "pageUp:";
  "^ " = "setMark:";
  "~b" = "moveWordBackward:";
  "~c" = "capitalizeWord:";
  "~d" = "deleteWordForward:";
  "~f" = "moveWordForward:";
  "~<" = "moveToBeginningOfDocument:";
  "~>" = "moveToEndOfDocument:";

  // Implemented by subclasses to cancel the current operation.
  "^g" = "cancelOperation:";

  // Implemented by subclasses to capitalize the word or words surrounding the insertion point or selection, expanding the selection if necessary.
  "~c" = "capitalizeWord:";

  // Implemented by subclasses to scroll the selection, whatever it is, inside its visible area.
  // "" = "centerSelectionInVisibleArea:";

  // Implemented by subclasses to change the case of a letter or letters in the selection, perhaps by opening a panel with capitalization options or by cycling through possible case combinations.
  // "" = "changeCaseOfLetter:";

  // Implemented by subclasses to complete an operation in progress or a partially constructed element.
  "~/" = "complete:";

  // Implemented by subclasses to delete the selection, if there is one, or a single element backward from the insertion point (a letter or character in text, for example).
  // "^h" = "deleteBackward:";

  // Implemented by subclasses to delete the selection, if there is one, or a single character backward from the insertion point.
  // "" = "deleteBackwardByDecomposingPreviousCharacter:";

  // Implemented by subclasses to delete the selection, if there is one, or a single element forward from the insertion point (a letter or character in text, for example).
  "^d" = "deleteForward:";

  // Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the beginning of a line (typically of text).
  // "" = "deleteToBeginningOfLine:";

  // Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the beginning of a paragraph of text.
  // "" = "deleteToBeginningOfParagraph:";

  // Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the end of a line (typically of text).
  "^k" = "deleteToEndOfLine:";

  // Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the end of a paragraph of text.
  // "" = "deleteToEndOfParagraph:";

  // Implemented by subclasses to delete the selection, if there is one, or all items from the insertion point to a previously placed mark, including the selection itself if not empty.
  "^w" = "deleteToMark:";

  // Implemented by subclasses to delete the selection, if there is one, or a single word backward from the insertion point.
  // "" = "deleteWordBackward:";

  // Implemented by subclasses to delete the selection, if there is one, or a single word forward from the insertion point.
  "~d" = "deleteWordForward:";

  // Implemented by subclasses to indent the selection or the insertion point if there is no selection.
  // "" = "indent:";

  // Implemented by subclasses to handle a backward tab.
  // "" = "insertBacktab:";

  // Implemented by subclasses to insert a container break (typically a page break) at the insertion point or selection, deleting the selection if there is one.
  // "" = "insertContainerBreak:";

  // Implemented by subclasses to insert a line break (as distinguished from a paragraph break) at the insertion point or selection, deleting the selection if there is one.
  // "" = "insertLineBreak:";

  // Implemented by subclasses to insert a newline character at the insertion point or selection, deleting the selection if there is one, or to end editing if the receiver is a text field or other field editor.
  // "" = "insertNewline:";

  // Implemented by subclasses to insert a line-break character at the insertion point or selection, deleting the selection if there is one.
  // "" = "insertNewlineIgnoringFieldEditor:";

  // Implemented by subclasses to insert a paragraph separator at the insertion point or selection, deleting the selection if there is one.
  // "" = "insertParagraphSeparator:";

  // Implemented by subclasses to insert a tab character at the insertion point or selection, deleting the selection if there is one, or to end editing if the receiver is a text field or other field editor.
  // "" = "insertTab:";

  // Implemented by subclasses to insert a tab character at the insertion point or selection, deleting the selection if there is one.
  // "" = "insertTabIgnoringFieldEditor:";

  // Implemented by subclasses to insert a double quote character at the insertion point without interference by automatic quote correction.
  // "" = "insertDoubleQuoteIgnoringSubstitution:";

  // Implemented by subclasses to insert a single quote character at the insertion point without interference by automatic quote correction.
  // "" = "insertSingleQuoteIgnoringSubstitution:";

  // Overridden by subclasses to insert the supplied string at the insertion point or selection, deleting the selection if there is one.
  // "" = "insertText:";

  // Implemented by subclasses to make lowercase every letter in the word or words surrounding the insertion point or selection, expanding the selection if necessary.
  "~l" = "lowercaseWord:";

  // Implemented by subclasses to move the selection or insertion point one element or character backward.
  "^b" = "moveBackward:";

  // Implemented by subclasses to expand or reduce either end of the selection backward by one element or character.
  // "" = "moveBackwardAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the beginning of the next paragraph, expanding or reducing the current selection.
  // "" = "moveParagraphForwardAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the beginning of the previous paragraph, expanding or reducing the current selection.
  // "" = "moveParagraphBackwardAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the beginning of the document, expanding or reducing the current selection.
  // "" = "moveToBeginningOfDocumentAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the end of the document, expanding or reducing the current selection.
  // "" = "moveToEndOfDocumentAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the beginning of the line, expanding or reducing the current selection.
  // "" = "moveToBeginningOfLineAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the end of the line, expanding or reducing the current selection.
  // "" = "moveToEndOfLineAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the beginning of the current paragraph, expanding or reducing the current selection.
  // "" = "moveToBeginningOfParagraphAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the end of the current paragraph, expanding or reducing the current selection.
  // "" = "moveToEndOfParagraphAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the left end of the line.
  // "" = "moveToLeftEndOfLine:";

  // Implemented by subclasses to move the selection or insertion point to the left end of the line, expanding or contracting the selection as required.
  // "" = "moveToLeftEndOfLineAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point to the right end of the line
  // "" = "moveToRightEndOfLine:";

  // Implemented by subclasses to move the selection or insertion point to the right end of the line, expanding or contracting the selection as required.
  // "" = "moveToRightEndOfLineAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one element or character down.
  "^n" = "moveDown:";

  // Implemented by subclasses to expand or reduce the top or bottom end of the selection downward by one element, character, or line (whichever is appropriate for text direction).
  // "" = "moveDownAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one element or character forward.
  "^f" = "moveForward:";

  // Implemented by subclasses to expand or reduce either end of the selection forward by one element or character.
  // "" = "moveForwardAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one element or character to the left.
  // "" = "moveLeft:";

  // Implemented by subclasses to expand or reduce either end of the selection to the left (display order) by one element or character.
  // "" = "moveLeftAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one element or character to the right.
  // "" = "moveRight:";

  // Implemented by subclasses to expand or reduce either end of the selection to the right (display order) by one element or character.
  // "" = "moveRightAndModifySelection:";

  // Implemented by subclasses to move the selection to the first element of the document or the insertion point to the beginning.
  "~<" = "moveToBeginningOfDocument:";

  // Implemented by subclasses to move the selection to the first element of the selected line or the insertion point to the beginning of the line.
  "^a" = "moveToBeginningOfLine:";

  // Implemented by subclasses to move the insertion point to the beginning of the selected paragraph.
  "~a" = "moveToBeginningOfParagraph:";

  // Implemented by subclasses to move the selection to the last element of the document or the insertion point to the end.
  "~>" = "moveToEndOfDocument:";

  // Implemented by subclasses to move the selection to the last element of the selected line or the insertion point to the end of the line.
  "^e" = "moveToEndOfLine:";

  // Implemented by subclasses to move the insertion point to the end of the selected paragraph.
  "~e" = "moveToEndOfParagraph:";

  // Implemented by subclasses to move the selection or insertion point one element or character up.
  "^p" = "moveUp:";

  // Implemented by subclasses to expand or reduce the top or bottom end of the selection upward by one element, character, or line (whichever is appropriate for text direction).
  // "" = "moveUpAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one word backward.
  "~b" = "moveWordBackward:";

  // Implemented by subclasses to expand or reduce either end of the selection backward by one whole word.
  // "" = "moveWordBackwardAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one word forward, in logical order.
  "~f" = "moveWordForward:";

  // Implemented by subclasses to expand or reduce either end of the selection forward by one whole word.
  // "" = "moveWordForwardAndModifySelection:";

  // Implemented by subclasses to move the selection or insertion point one word to the left, in display order.
  // "" = "moveWordLeft:";

  // Implemented by subclasses to move the selection or insertion point one word right.
  // "" = "moveWordRight:";

  // Implemented by subclasses to expand or reduce either end of the selection to the right by one whole word.
  // "" = "moveWordRightAndModifySelection:";

  // Implemented by subclasses to expand or reduce either end of the selection left by one whole word in display order.
  // "" = "moveWordLeftAndModifySelection:";

  // Implemented by subclasses to scroll the receiver down (or back) one page in its scroll view, also moving the insertion point to the top of the newly displayed page.
  "^v" = "pageDown:";

  // Implemented by subclasses to scroll the receiver down (or back) one page in its scroll view, also moving the insertion point to the top of the newly displayed page. The selection is expanded or contracted as required.
  // "" = "pageDownAndModifySelection:";

  // Implemented by subclasses to scroll the receiver up (or forward) one page in its scroll view, also moving the insertion point to the top of the newly displayed page.
  "~v" = "pageUp:";

  // Implemented by subclasses to scroll the receiver up (or forward) one page in its scroll view, also moving the insertion point to the top of the newly displayed page. The selection is expanded or contracted as necessary.
  // "" = "pageUpAndModifySelection:";

  // Implemented by subclasses to perform a Quick Look on the text cursor position, selection, or whatever is appropriate for your view.
  // "" = "quickLookPreviewItems:";

  // Implemented by subclasses to scroll the receiver to the beginning of the document, without changing the selection.
  // "" = "scrollToBeginningOfDocument:";

  // Implemented by subclasses to scroll the receiver to the end of the document, without changing the selection.
  // "" = "scrollToEndOfDocument:";

  // Implemented by subclasses to scroll the receiver one line up in its scroll view, without changing the selection.
  // "" = "scrollLineUp:";

  // Implemented by subclasses to scroll the receiver one line down in its scroll view, without changing the selection.
  // "" = "scrollLineDown:";

  // Implemented by subclasses to scroll the receiver one page up in its scroll view, without changing the selection.
  // "" = "scrollPageUp:";

  // Implemented by subclasses to scroll the receiver one page down in its scroll view, without changing the selection.
  // "" = "scrollPageDown:";

  // Implemented by subclasses to select all selectable elements.
  // "" = "selectAll:";

  // Implemented by subclasses to select all elements in the line or lines containing the selection or insertion point.
  // "" = "selectLine:";

  // Implemented by subclasses to select all paragraphs containing the selection or insertion point.
  // "" = "selectParagraph:";

  // Implemented by subclasses to select all items from the insertion point or selection to a previously placed mark, including the selection itself if not empty.
  // "" = "selectToMark:";

  // Implemented by subclasses to extend the selection to the nearest word boundaries outside it (up to, but not including, word delimiters).
  // "" = "selectWord:";

  // Implemented by subclasses to set a mark at the insertion point or selection, which is used by deleteToMark(_:) and selectToMark(_:).
  "^ " = "setMark:";

  // Implemented by subclasses to invoke the help system, displaying information relevant to the receiver and its current state.
  // "" = "showContextHelp:";

  // Swaps the mark and the selection or insertion point, so that what was marked is now the selection or insertion point, and what was the insertion point or selection is now the mark.
  // "^x^x" = "swapWithMark:";  // Further down.

  // Transposes the characters to either side of the insertion point and advances the insertion point past both of them. Does nothing to a selected range of text.
  // "" = "transpose:";

  // Transposes the words to either side of the insertion point and advances the insertion point past both of them. Does nothing to a selected range of text.
  // "" = "transposeWords:";

  // Implemented by subclasses to make uppercase every letter in the word or words surrounding the insertion point or selection, expanding the selection if necessary.
  "~u" = "uppercaseWord:";

  // Replaces the insertion point or selection with text from the kill buffer.
  "^y" = "yank:";

  // This method is used in the process of finding a target for an action method.
  // "" = "supplementalTarget:";

  "^x" = {
    "u" = "undo:"; /* C-x u Undo */
    "^x" = "swapWithMark:";
  };
}

// func cancelOperation(Any?)
// Implemented by subclasses to cancel the current operation.

// func capitalizeWord(Any?)
// Implemented by subclasses to capitalize the word or words surrounding the insertion point or selection, expanding the selection if necessary.

// func centerSelectionInVisibleArea(Any?)
// Implemented by subclasses to scroll the selection, whatever it is, inside its visible area.

// func changeCaseOfLetter(Any?)
// Implemented by subclasses to change the case of a letter or letters in the selection, perhaps by opening a panel with capitalization options or by cycling through possible case combinations.

// func complete(Any?)
// Implemented by subclasses to complete an operation in progress or a partially constructed element.

// func deleteBackward(Any?)
// Implemented by subclasses to delete the selection, if there is one, or a single element backward from the insertion point (a letter or character in text, for example).

// func deleteBackwardByDecomposingPreviousCharacter(Any?)
// Implemented by subclasses to delete the selection, if there is one, or a single character backward from the insertion point.

// func deleteForward(Any?)
// Implemented by subclasses to delete the selection, if there is one, or a single element forward from the insertion point (a letter or character in text, for example).

// func deleteToBeginningOfLine(Any?)
// Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the beginning of a line (typically of text).

// func deleteToBeginningOfParagraph(Any?)
// Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the beginning of a paragraph of text.

// func deleteToEndOfLine(Any?)
// Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the end of a line (typically of text).

// func deleteToEndOfParagraph(Any?)
// Implemented by subclasses to delete the selection, if there is one, or all text from the insertion point to the end of a paragraph of text.

// func deleteToMark(Any?)
// Implemented by subclasses to delete the selection, if there is one, or all items from the insertion point to a previously placed mark, including the selection itself if not empty.

// func deleteWordBackward(Any?)
// Implemented by subclasses to delete the selection, if there is one, or a single word backward from the insertion point.

// func deleteWordForward(Any?)
// Implemented by subclasses to delete the selection, if there is one, or a single word forward from the insertion point.

// func indent(Any?)
// Implemented by subclasses to indent the selection or the insertion point if there is no selection.

// func insertBacktab(Any?)
// Implemented by subclasses to handle a backward tab.

// func insertContainerBreak(Any?)
// Implemented by subclasses to insert a container break (typically a page break) at the insertion point or selection, deleting the selection if there is one.

// func insertLineBreak(Any?)
// Implemented by subclasses to insert a line break (as distinguished from a paragraph break) at the insertion point or selection, deleting the selection if there is one.

// func insertNewline(Any?)
// Implemented by subclasses to insert a newline character at the insertion point or selection, deleting the selection if there is one, or to end editing if the receiver is a text field or other field editor.

// func insertNewlineIgnoringFieldEditor(Any?)
// Implemented by subclasses to insert a line-break character at the insertion point or selection, deleting the selection if there is one.

// func insertParagraphSeparator(Any?)
// Implemented by subclasses to insert a paragraph separator at the insertion point or selection, deleting the selection if there is one.

// func insertTab(Any?)
// Implemented by subclasses to insert a tab character at the insertion point or selection, deleting the selection if there is one, or to end editing if the receiver is a text field or other field editor.

// func insertTabIgnoringFieldEditor(Any?)
// Implemented by subclasses to insert a tab character at the insertion point or selection, deleting the selection if there is one.

// func insertDoubleQuoteIgnoringSubstitution(Any?)
// Implemented by subclasses to insert a double quote character at the insertion point without interference by automatic quote correction.

// func insertSingleQuoteIgnoringSubstitution(Any?)
// Implemented by subclasses to insert a single quote character at the insertion point without interference by automatic quote correction.

// func insertText(Any)
// Overridden by subclasses to insert the supplied string at the insertion point or selection, deleting the selection if there is one.

// func lowercaseWord(Any?)
// Implemented by subclasses to make lowercase every letter in the word or words surrounding the insertion point or selection, expanding the selection if necessary.

// func moveBackward(Any?)
// Implemented by subclasses to move the selection or insertion point one element or character backward.

// func moveBackwardAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection backward by one element or character.

// func moveParagraphForwardAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the beginning of the next paragraph, expanding or reducing the current selection.

// func moveParagraphBackwardAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the beginning of the previous paragraph, expanding or reducing the current selection.

// func moveToBeginningOfDocumentAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the beginning of the document, expanding or reducing the current selection.

// func moveToEndOfDocumentAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the end of the document, expanding or reducing the current selection.

// func moveToBeginningOfLineAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the beginning of the line, expanding or reducing the current selection.

// func moveToEndOfLineAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the end of the line, expanding or reducing the current selection.

// func moveToBeginningOfParagraphAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the beginning of the current paragraph, expanding or reducing the current selection.

// func moveToEndOfParagraphAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the end of the current paragraph, expanding or reducing the current selection.

// func moveToLeftEndOfLine(Any?)
// Implemented by subclasses to move the selection or insertion point to the left end of the line.

// func moveToLeftEndOfLineAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the left end of the line, expanding or contracting the selection as required.

// func moveToRightEndOfLine(Any?)
// Implemented by subclasses to move the selection or insertion point to the right end of the line

// func moveToRightEndOfLineAndModifySelection(Any?)
// Implemented by subclasses to move the selection or insertion point to the right end of the line, expanding or contracting the selection as required.

// func moveDown(Any?)
// Implemented by subclasses to move the selection or insertion point one element or character down.

// func moveDownAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce the top or bottom end of the selection downward by one element, character, or line (whichever is appropriate for text direction).

// func moveForward(Any?)
// Implemented by subclasses to move the selection or insertion point one element or character forward.

// func moveForwardAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection forward by one element or character.

// func moveLeft(Any?)
// Implemented by subclasses to move the selection or insertion point one element or character to the left.

// func moveLeftAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection to the left (display order) by one element or character.

// func moveRight(Any?)
// Implemented by subclasses to move the selection or insertion point one element or character to the right.

// func moveRightAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection to the right (display order) by one element or character.

// func moveToBeginningOfDocument(Any?)
// Implemented by subclasses to move the selection to the first element of the document or the insertion point to the beginning.

// func moveToBeginningOfLine(Any?)
// Implemented by subclasses to move the selection to the first element of the selected line or the insertion point to the beginning of the line.

// func moveToBeginningOfParagraph(Any?)
// Implemented by subclasses to move the insertion point to the beginning of the selected paragraph.

// func moveToEndOfDocument(Any?)
// Implemented by subclasses to move the selection to the last element of the document or the insertion point to the end.

// func moveToEndOfLine(Any?)
// Implemented by subclasses to move the selection to the last element of the selected line or the insertion point to the end of the line.

// func moveToEndOfParagraph(Any?)
// Implemented by subclasses to move the insertion point to the end of the selected paragraph.

// func moveUp(Any?)
// Implemented by subclasses to move the selection or insertion point one element or character up.

// func moveUpAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce the top or bottom end of the selection upward by one element, character, or line (whichever is appropriate for text direction).

// func moveWordBackward(Any?)
// Implemented by subclasses to move the selection or insertion point one word backward.

// func moveWordBackwardAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection backward by one whole word.

// func moveWordForward(Any?)
// Implemented by subclasses to move the selection or insertion point one word forward, in logical order.

// func moveWordForwardAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection forward by one whole word.

// func moveWordLeft(Any?)
// Implemented by subclasses to move the selection or insertion point one word to the left, in display order.

// func moveWordRight(Any?)
// Implemented by subclasses to move the selection or insertion point one word right.

// func moveWordRightAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection to the right by one whole word.

// func moveWordLeftAndModifySelection(Any?)
// Implemented by subclasses to expand or reduce either end of the selection left by one whole word in display order.

// func pageDown(Any?)
// Implemented by subclasses to scroll the receiver down (or back) one page in its scroll view, also moving the insertion point to the top of the newly displayed page.

// func pageDownAndModifySelection(Any?)
// Implemented by subclasses to scroll the receiver down (or back) one page in its scroll view, also moving the insertion point to the top of the newly displayed page. The selection is expanded or contracted as required.

// func pageUp(Any?)
// Implemented by subclasses to scroll the receiver up (or forward) one page in its scroll view, also moving the insertion point to the top of the newly displayed page.

// func pageUpAndModifySelection(Any?)
// Implemented by subclasses to scroll the receiver up (or forward) one page in its scroll view, also moving the insertion point to the top of the newly displayed page. The selection is expanded or contracted as necessary.

// func quickLookPreviewItems(Any?)
// Implemented by subclasses to perform a Quick Look on the text cursor position, selection, or whatever is appropriate for your view.

// func scrollToBeginningOfDocument(Any?)
// Implemented by subclasses to scroll the receiver to the beginning of the document, without changing the selection.

// func scrollToEndOfDocument(Any?)
// Implemented by subclasses to scroll the receiver to the end of the document, without changing the selection.

// func scrollLineUp(Any?)
// Implemented by subclasses to scroll the receiver one line up in its scroll view, without changing the selection.

// func scrollLineDown(Any?)
// Implemented by subclasses to scroll the receiver one line down in its scroll view, without changing the selection.

// func scrollPageUp(Any?)
// Implemented by subclasses to scroll the receiver one page up in its scroll view, without changing the selection.

// func scrollPageDown(Any?)
// Implemented by subclasses to scroll the receiver one page down in its scroll view, without changing the selection.

// func selectAll(Any?)
// Implemented by subclasses to select all selectable elements.

// func selectLine(Any?)
// Implemented by subclasses to select all elements in the line or lines containing the selection or insertion point.

// func selectParagraph(Any?)
// Implemented by subclasses to select all paragraphs containing the selection or insertion point.

// func selectToMark(Any?)
// Implemented by subclasses to select all items from the insertion point or selection to a previously placed mark, including the selection itself if not empty.

// func selectWord(Any?)
// Implemented by subclasses to extend the selection to the nearest word boundaries outside it (up to, but not including, word delimiters).

// func setMark(Any?)
// Implemented by subclasses to set a mark at the insertion point or selection, which is used by deleteToMark(_:) and selectToMark(_:).

// func showContextHelp(Any?)
// Implemented by subclasses to invoke the help system, displaying information relevant to the receiver and its current state.

// func swapWithMark(Any?)
// Swaps the mark and the selection or insertion point, so that what was marked is now the selection or insertion point, and what was the insertion point or selection is now the mark.

// func transpose(Any?)
// Transposes the characters to either side of the insertion point and advances the insertion point past both of them. Does nothing to a selected range of text.

// func transposeWords(Any?)
// Transposes the words to either side of the insertion point and advances the insertion point past both of them. Does nothing to a selected range of text.

// func uppercaseWord(Any?)
// Implemented by subclasses to make uppercase every letter in the word or words surrounding the insertion point or selection, expanding the selection if necessary.

// func yank(Any?)
// Replaces the insertion point or selection with text from the kill buffer.

// func supplementalTarget(forAction: Selector, sender: Any?)
// This method is used in the process of finding a target for an action method.

;; -*- lexical-binding: t -*-
;; package mac-frame

;; Questions/TODOs:
;; 1. List frames on the current workspace (Space).

(defcustom mac-frame/default-frame-width 660
  "Default width for new frames.")

(defcustom mac-frame/default-frame-height 608
  "Default height for new frames.")

(defcustom mac-frame/horizontal-step 165
  "Default horizontal displacement length (in pixels) used when moving frames.")

(defcustom mac-frame/vertical-step 152
  "Default vertical displacement length (in pixels) used when moving frames.")

(defun mac-frame/.workarea-edges-currently-broken (frame)
  "This straightforward version of .workarea-edges is not working for me.
   Use the version that computes `cadr' of workarea via a workaround.
   Hopefully in a future version of macos or emacs the workarea values will
   be computed correctly, removing the need for a workaround."
  (let ((wa (alist-get 'workarea (frame-monitor-attributes frame))))
    `((left . ,(car wa))
      (top . ,(cadr wa))
      (width . ,(caddr wa))
      (height . ,(cadddr wa))
      (right . ,(+ (car wa) (caddr wa)))
      (bottom . ,(+ (cadr wa) (cadddr wa))))))

(defun mac-frame/.workarea-edges (frame)
  (let* ((attrs (frame-monitor-attributes frame))
         (geometry (alist-get 'geometry attrs))
         (workarea-orig (alist-get 'workarea attrs))
         (wa workarea-orig
             ;; Note: under some conditions workarea-orig is wrong for the
             ;; secondary screen (something to do with macos Dock and the top
             ;; menubar).  Replacing (cadr workarea-orig) with the following
             ;; calculation worked for hidden Dock for secondary screen.
             ;; TODO: explore the failure condition and create a workaround that
             ;; works for {primary screen, secondary screen} x
             ;; {Dock:hidden, Dock:bottom, Dock:left, Dock:right}.
             ;;
             ;; (list
             ;; (car workarea-orig)
             ;; ;; This is sometimes wrong for secondary monitors:
             ;; ;; (cadr workarea-orig).
             ;; ;; Having to work around it:
             ;; (+ (cadr geometry)
             ;;    (- (cadddr geometry) (cadddr workarea-orig)))
             ;; (caddr workarea-orig)
             ;; (cadddr workarea-orig))
             ))
    `((left . ,(car wa))
      (top . ,(cadr wa))
      (width . ,(caddr wa))
      (height . ,(cadddr wa))
      (right . ,(+ (car wa) (caddr wa)))
      (bottom . ,(+ (cadr wa) (cadddr wa))))))

(defun mac-frame/.frame-edges (frame)
  (let ((fe (ns-frame-edges frame 'outer-edges)))
    `((left . ,(car fe))
      (top . ,(cadr fe))
      (right . ,(caddr fe))
      (bottom . ,(cadddr fe))
      (width . ,(- (caddr fe) (car fe)))
      (height . ,(- (cadddr fe) (cadr fe))))))

(defun mac-frame/.bounds (frame)
  "Bounds rectangle (min-x, min-y, max-x, max-y).  When (x-left, y-top) of the
   frame is within this rectangle, the frame will lie within the workarea."
  (let* ((fe (mac-frame/.frame-edges frame))
         (wa (mac-frame/.workarea-edges frame)))
    `((left . ,(alist-get 'left wa))
      (top . ,(alist-get 'top wa))
      (right . ,(- (alist-get 'right wa) (alist-get 'width fe)))
      (bottom . ,(- (alist-get 'bottom wa) (alist-get 'height fe))))))

(defun mac-frame/.quantized-random (a b quantum)  ;; assumes a <= b
  "returns a random number among {a, a+q, a+2q, ...}, whose largest number is
   less than or equal to b."
  (if (= a b)
      a
    (+ a (* quantum (/ (random (- b a)) quantum)))))

(defun mac-frame/make-scratch-frame ()
  (interactive)
  (let* (;; Maybe put these into a custom parameter.
         (wa (mac-frame/.workarea-edges nil)))
    (display-buffer
     "*scratch*"
     `((display-buffer-pop-up-frame)
       (reusable-frames . 0)
       (inhibit-same-window . t)
       (pop-up-frame-parameters
        . ((left + ,(mac-frame/.quantized-random
                     (alist-get 'left wa)
                     (- (alist-get 'right wa)
                        mac-frame/default-frame-width)
                     mac-frame/horizontal-step))
           (top + ,(mac-frame/.quantized-random
                    (alist-get 'top wa)
                    (- (alist-get 'bottom wa)
                       mac-frame/default-frame-height)
                    mac-frame/vertical-step)))))
     nil)))

(defun mac-frame/make-tmp-frame ()
  (interactive)
  (switch-to-buffer-other-frame (get-buffer-create (make-temp-name "tmp "))))

(defun mac-frame/place-randomly ()
  (interactive)
  (let ((bounds (mac-frame/.bounds (selected-frame))))
    (modify-frame-parameters
     frame
     `((left + ,(mac-frame/.quantized-random (alist-get 'left bounds)
                                             (alist-get 'right bounds)
                                             mac-frame/horizontal-step))
       (top + ,(mac-frame/.quantized-random (alist-get 'top)
                                            (alist-get 'bottom)
                                            mac-frame/vertical-step))))))

(defun mac-frame/toggle-frame-vertical-expansion ()
  (interactive)
  (let* ((frame (selected-frame))
         (fe (mac-frame/.frame-edges frame))
         (wa (mac-frame/.workarea-edges frame))
         (p (frame-parameters frame))
         ;; 4.0 is perhaps the total height of the vertical box lines (around
         ;; modeline, etc.).
         (line-height (/ (- (frame-pixel-height) 4.0) (alist-get 'height p)))
         (max-height-lines (floor (/ (alist-get 'height wa) line-height))))
    (modify-frame-parameters
     frame
     (if (< 45 (alist-get 'height p))
         `((height . ,(or (alist-get 'last-height p) 35))
           (left + ,(alist-get 'left fe))
           (top + ,(or (alist-get 'last-top p) (alist-get 'top wa))))
       `((height . ,max-height-lines)
         (left + ,(alist-get 'left fe))
         (top + ,(alist-get 'top wa))
         (last-height . ,(alist-get 'height p))
         (last-top . ,(alist-get 'top fe)))))))

(defun mac-frame/.nudge (frame axis initial order-fn)
  (let* ((best initial)
         (fe (mac-frame/.frame-edges frame))
         (axis-beg (cl-case axis (:x 'left) (:y 'top)))
         (axis-end (cl-case axis (:x 'right) (:y 'bottom)))
         (fallback-step (cl-case axis
                          (:x mac-frame/horizontal-step)
                          (:y mac-frame/vertical-step)))
         (a0 (alist-get axis-beg fe))
         (b0 (alist-get axis-end fe)))
    (dolist (other-frame (alist-get 'frames (frame-monitor-attributes frame)))
      (let* ((other-fe (mac-frame/.frame-edges other-frame))
             (a (alist-get axis-beg other-fe))
             (b (alist-get axis-end other-fe)))
        (dolist (edge (list (- a (- b0 a0)) a (- b (- b0 a0)) b))
          (when (funcall order-fn best edge a0)
            (setq best edge)))))
    (unless (= a0 best)
      (modify-frame-parameters
       frame `((,axis-beg + ,(mac-frame/.nudge-towards
                              a0 best fallback-step)))))))

(defun mac-frame/.nudge-towards (a b threshold)
  (cond ((< b (- a threshold)) (- a threshold))
        ((> b (+ a threshold)) (+ a threshold))
        (t b)))

(defun mac-frame/nudge-frame-in-direction (frame direction)
  (let* ((frame (or frame (selected-frame)))
         (bounds (mac-frame/.bounds frame)))
    (cl-case direction
      (:left (mac-frame/.nudge frame :x (alist-get 'left bounds) '<))
      (:right (mac-frame/.nudge frame :x (alist-get 'right bounds) '>))
      (:up (mac-frame/.nudge frame :y (alist-get 'top bounds) '<))
      (:down (mac-frame/.nudge frame :y (alist-get 'bottom bounds) '>)))))

(defun mac-frame/nudger (dir)
  #'(lambda ()
      (interactive)
      (mac-frame/nudge-frame-in-direction (selected-frame) dir)))

(defun mac-frame/.secondary-overlap-ratio (axis current-fe other-fe)
  (let ((other-axis-beg (cl-case axis (:x 'top) (:y 'left)))
        (other-axis-end (cl-case axis (:x 'bottom) (:y 'right))))
    (/ (- (min (alist-get other-axis-end other-fe)
               (alist-get other-axis-end current-fe))
          (max (alist-get other-axis-beg other-fe)
               (alist-get other-axis-beg current-fe)))
       1.0
       (- (alist-get other-axis-end current-fe)
          (alist-get other-axis-beg current-fe)))))

(defun mac-frame/.select-directional
    (current-frame axis initial-edge order-fn all-frames)
  (let* ((axis-beg (cl-case axis (:x 'left) (:y 'top)))
         (best (cons initial-edge nil))
         (overlap-score-threshold 0.1)
         (best-overlap 0)
         (current-fe (mac-frame/.frame-edges current-frame))
         (current-edge (alist-get axis-beg current-fe)))
    (dolist (other-frame all-frames)
      (let* ((other-fe (mac-frame/.frame-edges other-frame))
             (other-edge (alist-get axis-beg other-fe))
             (overlap-score (mac-frame/.secondary-overlap-ratio
                             axis current-fe other-fe)))
        (cond ((eq other-frame current-frame)) ; skip.
              ((< overlap-score overlap-score-threshold)) ; skip.
              ((funcall order-fn (car best) other-edge current-edge)
               (setq best (cons other-edge other-frame)))
              ((and (funcall order-fn other-edge current-edge)
                    (= (car best) other-edge)
                    (> overlap-score best-overlap))
               (setq best (cons other-edge other-frame))
               (setq best-overlap overlap-score)))))
    (when (cdr best)
      (select-frame-set-input-focus (cdr best)))))

(defun mac-frame/select-frame-in-direction (frame direction)
  (let* ((frames (alist-get 'frames (frame-monitor-attributes frame))))
    (cl-case direction
      (:left (mac-frame/.select-directional frame :x -99999 '< frames))
      (:right (mac-frame/.select-directional frame :x 99999 '> (reverse
                                                                frames)))
      (:up (mac-frame/.select-directional frame :y -99999 '< frames))
      (:down (mac-frame/.select-directional frame :y 99999 '> (reverse
                                                               frames))))))

(defun mac-frame/directional-selector (dir)
  #'(lambda ()
      (interactive)
      (mac-frame/select-frame-in-direction (selected-frame) dir)))

(defun mac-frame/vertframe (n)
  (interactive "nNumber of vertical divisions: ")
  (make-frame)
  (set-frame-parameter nil 'width (- (* n 83) 3))
  (dotimes (i (- n 1))
    (split-window-right))
  (balance-windows)
  (mac-frame/toggle-frame-vertical-expansion))

;; TODO: Iterations should happen when the Command key is held down and ` is
;; typed repeatedly, not when Command-` is typed followed by more ` keys.
(defun mac-frame/select-mru ()
  (interactive)
  (let ((keymap (make-sparse-keymap))
        (frames (cdr (frame-list-z-order))))
    (when frames
      (select-frame-set-input-focus (pop frames))

      (define-key keymap (kbd "`")
                  (lambda () (interactive)
                    (when frames (select-frame-set-input-focus (pop frames)))))
      (set-transient-map keymap t))))

(defun mac-frame/clone-frame ()
  (interactive)
  (let ((fe (mac-frame/.frame-edges nil)))
    (make-frame `((top + ,(alist-get 'top fe))
                  (left + ,(+ (alist-get 'left fe)
                              mac-frame/horizontal-step))))))

(global-set-key (kbd "s--") 'mac-frame/place-randomly)
(global-set-key (kbd "s-<left>")  (mac-frame/directional-selector :left))
(global-set-key (kbd "s-<right>") (mac-frame/directional-selector :right))
(global-set-key (kbd "s-<up>")    (mac-frame/directional-selector :up))
(global-set-key (kbd "s-<down>")  (mac-frame/directional-selector :down))
(global-set-key (kbd "s-S-<left>")  (mac-frame/nudger :left))
(global-set-key (kbd "s-S-<right>") (mac-frame/nudger :right))
(global-set-key (kbd "s-S-<up>")    (mac-frame/nudger :up))
(global-set-key (kbd "s-S-<down>")  (mac-frame/nudger :down))
(global-set-key (kbd "s-b") (mac-frame/directional-selector :left))
(global-set-key (kbd "s-f") (mac-frame/directional-selector :right))
(global-set-key (kbd "s-p") (mac-frame/directional-selector :up))
(global-set-key (kbd "s-n") (mac-frame/directional-selector :down))
(global-set-key (kbd "s-B") (mac-frame/nudger :left))
(global-set-key (kbd "s-F") (mac-frame/nudger :right))
(global-set-key (kbd "s-P") (mac-frame/nudger :up))
(global-set-key (kbd "s-N") (mac-frame/nudger :down))
(global-set-key (kbd "s-=") 'toggle-frame-maximized)
(global-set-key (kbd "s-\\") 'mac-frame/toggle-frame-vertical-expansion)
(global-set-key (kbd "s-t") 'mac-frame/make-scratch-frame)
(global-set-key (kbd "s-;") 'mac-frame/make-scratch-frame)
(global-set-key (kbd "s-`") 'mac-frame/select-mru)
(global-set-key (kbd "s-d") prefix-arg)
(global-set-key (kbd "s-d t") 'mac-frame/make-tmp-frame)
(global-set-key (kbd "s-d d") 'mac-frame/clone-frame)
(global-set-key (kbd "C-s-f") 'toggle-frame-fullscreen)

;; s-p was ns-print-buffer, s-t was the font thing

(provide 'mac-frame)

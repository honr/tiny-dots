;; stash-files

(defun stash-files--get-window-desktop (buffer)
  "Get the desktop number of the window associated with BUFFER."
  (when-let ((window (get-buffer-window buffer t))
             (property-string (x-window-property
                               "_NET_WM_DESKTOP"
                               (window-frame window)
                               "CARDINAL"))
             (n 0))
    (dolist (x (reverse (string-to-list property-string)))
      (setq n (+ (ash n 8) x)))
    n))

(defun stash-files--get-desktop-names ()
  (cl-remove-if #'string-empty-p
                (string-split
                 (x-window-property "_NET_DESKTOP_NAMES" nil "UTF8_STRING" 0)
                 "\0")))

(defun stash-files-dump (filename)
  "Dump the list of open files, grouped by desktop, to FILENAME."
  (interactive "FOutput file: ")
  (when-let ((desktop-names (stash-files--get-desktop-names)))
    (let ((m (make-hash-table :test 'equal))
          (hidden ".hidden"))
      (dolist (buffer (buffer-list))
        (when-let ((f (buffer-file-name buffer)))
          (let ((d (if-let ((i (stash-files--get-window-desktop buffer)))
                       (nth i desktop-names)
                     hidden)))
            (puthash d (append (or (gethash d m) '())
                               (list f))
                     m))))
      (with-temp-file filename
        (insert (format "# Comment out lines with #\n" desktop-names))
        (dolist (desktop (append desktop-names (list hidden)))
          (insert (format "\n** desktop %s\n" desktop))
          (dolist (file (sort (gethash desktop m) 'string<))
            (insert (format "  %s\n" file))))))))

;; (stash-files-dump (expand-file-name "~/.history/emacs-files"))

(defun stash-files-dump-flat (filename)
  "Dump the list of open files to FILENAME."
  (interactive "FOutput file: ")
  (with-temp-file filename
    (dolist (buffer (buffer-list))
      (when (buffer-file-name buffer)
        (insert (buffer-file-name buffer) "\n")))))

;; (stash-files-dump-flat (expand-file-name "~/.history/emacs-files-flat"))

;; TODO: Create (or move) each frame in the corresponding desktop.
(defun stash-files-visit (filename)
  "Visit files listed in FILENAME, one per line (skipping lines starting with # or ;)."
  (interactive "fVisit files from: ")
  (with-temp-buffer
    (insert-file-contents filename)
    (goto-char (point-min))
    (while (not (eobp))
      (let ((line (buffer-substring-no-properties (point-min) (line-end-position))))
        (when (and line
                   (not (string-match "^[#;*]" line))
                   (not (string-match "^[ \t]*$" line))) ;skip empty lines.
          (let ((trimmed-line (string-trim line))) ;remove leading and trailing whitespace.
            (find-file-noselect trimmed-line))))
      (forward-line 1))))


(provide 'stash-files)

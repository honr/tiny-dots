;;; -*- lexical-binding: t -*-

(require 'lib-sexp)

(defun toggle-window-split-direction ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

(defun buffer-file-name-as-kill ()
  "Copy the name of the file name of current buffer to the kill-ring"
  (interactive)
  (cond (buffer-file-name (kill-new buffer-file-name))
        (list-buffers-directory (kill-new list-buffers-directory))))

(with-eval-after-load 'calendar
  (defun calendar-cursor-as-kill ()
    (interactive)
    (kill-new (calendar-date-string (calendar-cursor-to-date t))))
  (add-hook
   'calendar-mode-hook
   (lambda ()
     (local-set-key (kbd "w") 'calendar-cursor-as-kill))))

(with-eval-after-load 'comint
 (define-key comint-mode-map (kbd "M-p")
             'comint-previous-matching-input-from-input)
 (define-key comint-mode-map (kbd "M-n")
             'comint-next-matching-input-from-input)
 (define-key comint-mode-map (kbd "C-c M-p") 'comint-previous-input)
 (define-key comint-mode-map (kbd "C-c M-n") 'comint-next-input))

(define-key minibuffer-local-map (kbd "M-p") 'previous-complete-history-element)
(define-key minibuffer-local-map (kbd "M-n") 'next-complete-history-element)
(define-key minibuffer-local-map (kbd "<up>")
            'previous-complete-history-element)
(define-key minibuffer-local-map (kbd "<down>") 'next-complete-history-element)

(defun delete-frame-and-buffer ()
  "Kill the current buffer and kill the frame containing it"
  (interactive)
  (kill-buffer (current-buffer))
  (delete-frame))
(global-set-key "\C-x5k" 'delete-frame-and-buffer)

(defvar-local keyword-delimiter "_")
(dolist (mode '(emacs-lisp-mode-hook
                lisp-mode-hook
                clojure-mode-hook))
  (add-hook mode (lambda () (setq keyword-delimiter "-"))))
(global-set-key (kbd "S-SPC")
                (lambda (arg)
                  (interactive "p")
                  (if (integerp arg)
                      (if (> arg 0)
                          (dotimes (_ arg)
                            (insert keyword-delimiter))
                        (let ((c (read-string "Set keyword-delimiter to: ")))
                          (setq keyword-delimiter c)
                          (message (format "keyword-delimiter is now '%s'."
                                           keyword-delimiter))))
                    (insert keyword-delimiter))))

;; Shift and transpose.
(global-set-key (kbd "C-S-f") (lambda () (interactive) (transpose-chars 1)))
(global-set-key (kbd "C-S-b") (lambda () (interactive) (transpose-chars -1)))
(global-set-key (kbd "M-F") (lambda () (interactive) (transpose-words 1)))
(global-set-key (kbd "M-B") (lambda () (interactive) (transpose-words -1)))
(global-set-key (kbd "M-E") (lambda () (interactive) (transpose-sentences 1)))
(global-set-key (kbd "M-A") (lambda () (interactive) (transpose-sentences -1)))
(global-set-key (kbd "C-S-n") (lambda () (interactive) (transpose-lines 1)))
(global-set-key (kbd "C-S-p") (lambda () (interactive) (transpose-lines -1)))
(global-set-key (kbd "M-C-S-f") (lambda () (interactive) (transpose-sexps 1)))
(global-set-key (kbd "M-C-S-b") (lambda () (interactive) (transpose-sexps -1)))

(defun dired-apply-open (&optional arg filelist)
  "Apply the program \"open\" to filelist.
With an `arg', applies the `arg' instead of \"open\" to the filelist."
  (interactive
   (let ((files (dired-get-marked-files t current-prefix-arg)))
     (list (when current-prefix-arg
             (dired-read-shell-command
              (concat "! on "
                      "%s: ")
              current-prefix-arg
              files))
           files)))
  (apply 'call-process (or arg "/usr/bin/open") nil 0 nil filelist))

(with-eval-after-load 'dired
 (define-key dired-mode-map (kbd "C-;") 'dired-apply-open))
(with-eval-after-load 'wdired
  (define-key wdired-mode-map (kbd "C-;") 'dired-apply-open))

(defun terminal-here--build-remote-command (default-directory buffer-file-name)
  (when (or (string-prefix-p "/sshx:" default-directory)
            (string-prefix-p "/ssh:" default-directory)
            (string-prefix-p "/rsync:" default-directory)
            (string-prefix-p "/scp:" default-directory)
            (string-prefix-p "/s:" default-directory))
    (let* ((abc (split-string default-directory ":"))
           (ignored--remote-pseudo-protocol (car abc)) ; -ignored
           (remote-host (cadr abc))                    ; remote
           (remote-path (caddr abc))                   ; path on the remote
           (remote-filename (when buffer-file-name
                              (substring
                               (caddr (split-string buffer-file-name ":"))
                               (length remote-path)))))
      ;; CAVEAT: assumes remote-path and remote-filename are single shell
      ;; tokens, and have no '$' in them.
      (format "ssh '%s' -t 'cd %s; %s%s'"
              remote-host
              remote-path
              (if remote-filename (format "F=%s " remote-filename) "")
              shell-file-name))))

(defun terminal-here ()
  (interactive)
  (if (and (equal 'darwin system-type) (equal 'ns window-system))
      (ns-do-applescript
       (concat
        "tell application \"Terminal\"\n"
        "activate\n"
        (format "do script \"%s\"\n"
                (or (terminal-here--build-remote-command
                     default-directory buffer-file-name)
                    (format "cd '%s'"
                            (expand-file-name default-directory))))
        "end tell"))
    ;; Under a terminal. For darwin we assume this is under Xquartz.
    (start-process-shell-command
     "external-xterm" nil
     (if-let ((rem (terminal-here--build-remote-command
                    default-directory buffer-file-name)))
       (format "alacritty -e \"%s\"" rem)
       (format "alacritty -e '%s'" shell-file-name)))))

(defun external-directory-browser-here ()
  (interactive)
  (start-process-shell-command "file-browser" nil "/usr/bin/open ."))

(defun frame-set-internal-border (arg)
  (interactive "nEnter frame inter border width: ")
  (modify-frame-parameters nil (list (cons 'internal-border-width arg))))

(defun face-and-theme-adjust (frame-arg)
  "Modify face and theme for all frames.  When frame-arg is
non-nil, adjustments (except for theme changes) will be made to
the selected frame."
  (interactive "P")
  (let ((map (make-sparse-keymap))
        (frame (when frame-arg (selected-frame)))
        (default-face-height 140)
        (inc 10)
        ;; (echo-keystrokes nil)
        (face 'default))

    (dolist (key (list (kbd "+") (kbd "=")))
      (define-key map key
                  (lambda () (interactive)
                    (set-face-attribute face frame :height
                                        (+ (face-attribute face :height) inc))
                    (message "face height increased to %d"
                             (face-attribute face :height)))))
    (define-key map (kbd "-")
                (lambda () (interactive)
                  (set-face-attribute face frame :height
                                      (- (face-attribute face :height) inc))
                  (message "face height decreased to %d"
                           (face-attribute face :height))))
    (define-key map (kbd "0")
                (lambda () (interactive)
                  (set-face-attribute face frame :height default-face-height)
                  (message "Face height set to to %d"
                           (face-attribute face :height))))
    (define-key map (kbd "c")
                (lambda () (interactive)
                  (let ((x (car (window-margins))))
                    (set-window-margins
                     nil
                     (if (and (numberp x) (< 0 x))
                         0
                       (/ (- (window-width) fill-column) 2))))))
    (dolist (key-and-theme
             (list
              (list (kbd "l") :light 'plain-light)
              (list (kbd "d") :dark 'plain-dark)
              (list (kbd "G") :light 'plain-grey)
              (list (kbd "g") :dark 'plain-grey)
              (list (kbd "b") :light 'light-balcony)
              (list (kbd "t") :light 'flat-white)
              (list (kbd "w") :light 'whitestone)
              (list (kbd "W") :light 'whitestone-text)
              (list (kbd "f") :dark 'dark-forge)
              (list (kbd "F") :dark 'fruitsalad-dark)
              (list (kbd "r") :dark 'dark-ruthless)))
      (let ((k (car key-and-theme))
            (mode (cadr key-and-theme))
            (theme (caddr key-and-theme)))
        (define-key
         map k (lambda () (interactive)
                 (setq custom-enabled-themes nil)
                 (load-theme theme)
                 (face-and-theme-macos-set-dark-mode mode)
                 (message "Active themes are: %s" custom-enabled-themes)))))

    (let* ((map-exit-fn (set-transient-map map t)))
      (message "Typeface height: %s, themes: %s.  Adjust further"
               (face-attribute face :height) custom-enabled-themes)

      (define-key map (kbd "RET")
                  (lambda () (interactive) (funcall map-exit-fn)))

      (define-key map (kbd ".")
                  (lambda (fringe-width)
                    ;; Instead of (interactive "nPROMPT") this form allows us to
                    ;; terminate the transient map.
                    (interactive
                     (progn
                       (funcall map-exit-fn)
                       (list (read-number "Enter new fringe-width: "))))
                    (set-window-fringes nil fringe-width fringe-width)
                    (message "Current frame's fringe-width set to %d"
                             fringe-width)))

      (define-key map (kbd ",")
                  (lambda (opacity-active opacity-inactive)
                    (interactive
                     (progn
                       (funcall map-exit-fn)
                       (list
                        (read-number "Opacity for new active frames: ")
                        (read-number "Opacity for new inactive frames: "))))
                    (setf (map-elt default-frame-alist 'alpha)
                          (list opacity-active opacity-inactive))))

      (define-key map (kbd ":")
                  (lambda (border-width)
                    (interactive
                     (progn
                       (funcall map-exit-fn)
                       (list
                        (read-number "Enter frame internal border width: "))))
                    (modify-frame-parameters
                     nil (list (cons 'internal-border-width border-width))))))))

(defun face-and-theme-set-face (arg)
  (interactive "sEnter typeface family: ")
  (set-face-attribute 'default nil :family arg)
  (message "Face family set to %s"
           (face-attribute 'default :family)))

(defun face-and-theme-set-face-height (arg)
  (interactive "nEnter typeface height: ")
  (set-face-attribute 'default nil :height arg)
  (message "Face height set to %s"
           (face-attribute 'default :height)))

(defun face-and-theme-set-macos-terminal-face (arg)
  (interactive "sEnter Terminal.app profile: ")
  (ns-do-applescript
   (concat
    "tell application \"Terminal\" to set default settings to "
    "settings set " (format "\"%s\"\n" arg)))
  (message "Face Terminal.app theme to %s" arg))

(defun face-and-theme-macos-set-dark-mode (arg)
  (let ((mode (cond ((eq arg :dark) "true")
                    ((eq arg :light) "false")
                    ((eq arg :toggle) "not dark mode"))))
    (when (and mode (equal 'darwin system-type))
      (ns-do-applescript (concat "tell application \"System Events\" to "
                                 "tell appearance preferences to "
                                 "set dark mode to " mode)))))

(defun define-semicolon-prefix-keys ()
  (interactive)

  ;; Remove a few local bindings that would overshadow the global definition
  ;; of this prefix key (";").
  (dolist (keymap (list paredit-mode-map
                        java-mode-map
                        c++-mode-map
                        c-mode-map
                        c-mode-base-map
                        term-mode-map))
    (define-key keymap (kbd ";") nil))

  (global-set-key (kbd ";") prefix-arg)
  (global-set-key (kbd "; SPC") (inserter-command "; "))
  (global-set-key (kbd "; ;") (inserter-command ";"))
  (global-set-key (kbd "; :") (inserter-command ";; "))
  (global-set-key (kbd "; RET") (lambda () (interactive)
                                  (insert ";") (newline)))
  (global-set-key (kbd "; C-j") (lambda () (interactive)
                                  (insert ";")
                                  (electric-newline-and-maybe-indent)))

  ;; TODO: Read numbers as prefix numeric args.
  (global-set-key (kbd "; l") 'execute-extended-command)
  (global-set-key (kbd "; u") 'undo)
  (global-set-key (kbd "; s") 'isearch-forward-regexp)
  (global-set-key (kbd "; S") 'isearch-forward)
  (global-set-key (kbd "; r") 'isearch-backward-regexp)
  (global-set-key (kbd "; R") 'isearch-backward)
  (global-set-key (kbd "; q") 'query-replace-regexp)
  (global-set-key (kbd "; Q") 'query-replace)
  (global-set-key (kbd "; v") 'switch-to-buffer)
  (global-set-key (kbd "; b") 'list-buffers)

  (progn
    (global-set-key (kbd "; n") prefix-arg)
    (global-set-key (kbd "; n b") 'org-narrow-to-block)
    (global-set-key (kbd "; n d") 'narrow-to-defun)
    (global-set-key (kbd "; n e") 'org-narrow-to-element)
    (global-set-key (kbd "; n n") 'narrow-to-region)
    (global-set-key (kbd "; n p") 'narrow-to-page)
    (global-set-key (kbd "; n s") 'org-narrow-to-subtree)
    (global-set-key (kbd "; n w") 'widen))

  (progn
    (global-set-key (kbd "; d") prefix-arg)
    (global-set-key (kbd "; d t") 'terminal-here)
    (when (fboundp 'magit-status)
      (global-set-key (kbd "; d m") 'magit-status))
    (global-set-key (kbd "; d e") 'erc-track-switch-buffer))

  (progn
    (global-set-key (kbd "; f") prefix-arg)
    (global-set-key (kbd "; f a") 'ffap-other-window)
    (global-set-key (kbd "; f c") 'calendar)
    (global-set-key (kbd "; f d") 'find-dired)
    (global-set-key (kbd "; f f") 'find-file)
    (global-set-key (kbd "; f F") 'find-file-read-only)
    (global-set-key (kbd "; f g") 'rgrep)
    (global-set-key (kbd "; f h") (find-file-dir-command "~/"))
    (global-set-key (kbd "; f j") 'dired-jump)
    (global-set-key (kbd "; f k") 'kill-buffer)
    (global-set-key (kbd "; f l") nil)
    (global-set-key (kbd "; f m") nil)
    (global-set-key (kbd "; f n") 'ffap-other-frame)
    (global-set-key (kbd "; f o") 'ff-find-other-file)
    (global-set-key (kbd "; f p") 'find-file-at-point)
    (global-set-key (kbd "; f q") nil)
    (global-set-key (kbd "; f r") 'ff-find-related-file)
    (global-set-key (kbd "; f s") 'save-buffer)
    (global-set-key (kbd "; f t") (find-file-dir-command "~/Arc/tea/"))
    (global-set-key (kbd "; f u") (find-file-dir-command "~/Arc/tea/scratch"))
    (global-set-key (kbd "; f v") 'view-mode)
    (global-set-key (kbd "; f w") 'buffer-file-name-as-kill)
    (global-set-key (kbd "; f y") nil)
    (global-set-key (kbd "; f z") 'server-edit)
    (global-set-key (kbd "; f 3") 'server-edit))

  (progn
    (global-set-key (kbd "; a") prefix-arg)
    (global-set-key (kbd "; a b") 'browse-url)
    (global-set-key (kbd "; a S-b") 'browse-url-at-point)
    (global-set-key (kbd "; a c") 'calc)
    (global-set-key (kbd "; a d") 'dict)
    (global-set-key (kbd "; a e") 'compile)
    (global-set-key (kbd "; a g") 'goto-line)
    ;; (global-set-key (kbd "; a q") 'comment-or-uncomment-line)
    ;; (global-set-key (kbd "; a w") 'comment-region-and-duplicate)
    (global-set-key (kbd "; a h") (joiner-command 'join-aaBb))
    (global-set-key (kbd "; a j") (joiner-command 'join-aa-bb))
    (global-set-key (kbd "; a k") (joiner-command 'join-AaBb))
    (global-set-key (kbd "; a l") (joiner-command 'join-aa_bb))
    (global-set-key (kbd "; a u") (joiner-command 'join-AA_BB))
    (global-set-key (kbd "; a s") (joiner-command 'join-aa-space-bb))
    (global-set-key (kbd "; a n") (joiner-command 'join-Aa-space-Bb)))

  (progn
    (global-set-key (kbd "; w") prefix-arg)
    (global-set-key (kbd "; w f") 'windmove-right)
    (global-set-key (kbd "; w b") 'windmove-left)
    (global-set-key (kbd "; w n") 'windmove-down)
    (global-set-key (kbd "; w p") 'windmove-up)
    (global-set-key (kbd "; w d") 'make-frame-command)
    (global-set-key (kbd "; w s") 'switch-to-buffer-other-frame)
    (global-set-key (kbd "; w c") 'clone-indirect-buffer-other-window)
    (global-set-key (kbd "; w t") 'toggle-window-split-direction)
    (global-set-key (kbd "; w l") 'move-to-window-line-top-bottom)
    (global-set-key (kbd "; w k") 'kill-buffer-and-window)
    (global-set-key (kbd "; w q") 'delete-frame-and-buffer)
    (global-set-key (kbd "; w 0") 'delete-window)
    (global-set-key (kbd "; w 1") 'delete-other-windows)
    (global-set-key (kbd "; w e") 'delete-other-frames)
    (global-set-key (kbd "; w 2") 'split-window-below)
    (global-set-key (kbd "; w 3") 'split-window-right)
    (global-set-key (kbd "; w o") 'other-window)
    (global-set-key (kbd "; w v") 'enlarge-window)
    (global-set-key (kbd "; w h") 'enlarge-window-horizontally)
    ;; C-x 5 .         xref-find-definitions-other-frame
    ;; C-x 5 0         delete-frame
    ;; C-x 5 1         delete-other-frames
    ;; C-x 5 b         switch-to-buffer-other-frame
    ;; C-x 5 f         find-file-other-frame
    ;; C-x 5 k         delete-frame-and-buffer
    (global-set-key (kbd "; w -") 'shrink-window-if-larger-than-buffer)
    (global-set-key (kbd "; w =") 'balance-windows))

  (progn
    (global-set-key (kbd "; h") prefix-arg)
    (define-key global-map (kbd "; h") 'help-command))

  (progn ;; Org-mode
    (global-set-key (kbd "; o") prefix-arg)
    (global-set-key (kbd "; o a") 'org-agenda)
    (with-eval-after-load 'org
      (define-key org-mode-map (kbd "; o e") 'org-export-dispatch)
      (define-key org-mode-map (kbd "; o s") 'org-insert-structure-template)))

  (progn
    (global-set-key (kbd "; j") 'face-and-theme-adjust))

  ;; A set of symbols:
  (progn
    (global-set-key (kbd "; g") prefix-arg)
    (global-set-key (kbd "; g a") "α")
    (global-set-key (kbd "; g b") "β")
    (global-set-key (kbd "; g m") "µ")
    (global-set-key (kbd "; g t") "θ")
    (global-set-key (kbd "; g g") "γ")
    (global-set-key (kbd "; g d") "δ")
    (global-set-key (kbd "; g e") "ε")
    (global-set-key (kbd "; g z") "ζ")
    (global-set-key (kbd "; g h") "η")
    (global-set-key (kbd "; g q") "θ")
    (global-set-key (kbd "; g i") "ι")
    (global-set-key (kbd "; g k") "κ")
    (global-set-key (kbd "; g l") "λ")
    (global-set-key (kbd "; g m") "μ")
    (global-set-key (kbd "; g n") "ν")
    (global-set-key (kbd "; g c") "ξ")
    (global-set-key (kbd "; g o") "ο")
    (global-set-key (kbd "; g p") "π")
    (global-set-key (kbd "; g r") "ρ")
    ;; ς
    (global-set-key (kbd "; g s") "σ")
    (global-set-key (kbd "; g t") "τ")
    (global-set-key (kbd "; g y") "υ")
    (global-set-key (kbd "; g f") "φ")
    (global-set-key (kbd "; g x") "χ")
    (global-set-key (kbd "; g u") "ψ")
    (global-set-key (kbd "; g w") "ω")

    (global-set-key (kbd "; g A") "Α")
    (global-set-key (kbd "; g B") "Β")
    (global-set-key (kbd "; g G") "Γ")
    (global-set-key (kbd "; g D") "Δ")
    (global-set-key (kbd "; g E") "Ε")
    (global-set-key (kbd "; g Z") "Ζ")
    (global-set-key (kbd "; g H") "Η")
    (global-set-key (kbd "; g Q") "Θ")
    (global-set-key (kbd "; g I") "Ι")
    (global-set-key (kbd "; g K") "Κ")
    (global-set-key (kbd "; g L") "Λ")
    (global-set-key (kbd "; g M") "Μ")
    (global-set-key (kbd "; g N") "Ν")
    (global-set-key (kbd "; g C") "Ξ")
    (global-set-key (kbd "; g O") "Ο")
    (global-set-key (kbd "; g P") "Π")
    (global-set-key (kbd "; g R") "Ρ")
    ;;
    (global-set-key (kbd "; g S") "Σ")
    (global-set-key (kbd "; g T") "Τ")
    (global-set-key (kbd "; g Y") "Υ")
    (global-set-key (kbd "; g F") "Φ")
    (global-set-key (kbd "; g X") "Χ")
    (global-set-key (kbd "; g U") "Ψ")
    (global-set-key (kbd "; g W") "Ω")

    (global-set-key (kbd "; g \"")
                    (lambda () (interactive) (insert "“”") (forward-char -1)))
    (global-set-key (kbd "; g '")
                    (lambda () (interactive) (insert "‘’") (forward-char -1)))
    (global-set-key (kbd "; g <") "≤")
    (global-set-key (kbd "; g >") "≥")
    (global-set-key (kbd "; g \\") "║")
    (global-set-key (kbd "; g /") (inserter-command "¦"))
    (global-set-key (kbd "; g .") "•")
    (global-set-key (kbd "; g `") (inserter-command "º"))
    (global-set-key (kbd "; g ,") "◊"))

  (progn
    (global-set-key (kbd "; i") 'insert-char)
    (global-set-key (kbd "; I") 'emoji-insert))

  (progn
    ;; Currently unused:
    (global-set-key (kbd "; c") nil)
    (global-set-key (kbd "; e") nil)
    (global-set-key (kbd "; k") nil)
    (global-set-key (kbd "; m") nil)
    (global-set-key (kbd "; p") nil)
    (global-set-key (kbd "; t") nil)
    (global-set-key (kbd "; x") nil)
    (global-set-key (kbd "; y") nil)
    (global-set-key (kbd "; z") nil)
    (global-set-key (kbd "; .") nil)
    (global-set-key (kbd "; ,") nil)))

(define-semicolon-prefix-keys)

(with-eval-after-load 'ibuffer
  (setq ibuffer-auto-mode t)
  (global-set-key (kbd "C-x C-b") 'ibuffer)
  (global-set-key (kbd "; B") 'ibuffer))

(provide 'config-keybindings)

;; whitestone-theme.el --- custom theme for faces  -*- lexical-binding: t; -*-

(deftheme whitestone
  "A serious looking black on white theme.")

(custom-theme-set-faces
 'whitestone
 `(default                       ((t (:foreground "#222222" :background "#ffffff"))))
 `(cursor                        ((t (:background "#66cc33"))))
 `(font-lock-builtin-face        ((t (:foreground "#a20"))))
 `(font-lock-comment-face        ((t (:foreground "#999999" :italic t))))
 `(font-lock-constant-face       ((t (:foreground "#a20"))))
 `(font-lock-doc-face            ((t (:foreground "#a20" :background "#eeeeee"))))
 `(font-lock-function-name-face  ((t (:foreground "#000" :weight bold))))
 `(font-lock-keyword-face        ((t (:foreground "#610"))))
 `(font-lock-preprocessor-face   ((t (:foreground "#610"))))
 `(font-lock-string-face         ((t (:foreground "#666666" :background "#eeeeee"))))
 `(font-lock-type-face           ((t (:foreground "#a20"))))
 `(font-lock-variable-name-face  ((t (:foreground "#222222" :weight bold))))
 `(font-lock-warning-face        ((t (:foreground "#d03" :weight bold))))

 `(dired-directory               ((t (:foreground "#06f"))))
 `(dired-symlink                 ((t (:foreground "#808"))))
 `(dired-ignored                 ((t (:foreground "#888"))))
 `(dired-marked                  ((t (:foreground "#080" :weight bold))))
 `(dired-flagged                 ((t (:foreground "#ffffff" :background "#990000" :weight bold))))
 `(dired-perm-write              ((t (:foreground "#a20" :weight bold))))
 `(scroll-bar                    ((t (:background "#dddddd"))))
 `(fringe                        ((t (:foreground "#bbbbbb" :background "#ffffff"))))
 `(header-line                   ((t (:foreground "#888" :background "#dddddd"))))
 `(highlight                     ((t (:foreground "light blue" :background "#dddddd"))))
 ;; (highline-face               ((t (:background "SeaGreen")))) ;;
 `(holiday-face                  ((t (:foreground "#777" :background "#000"))))
 `(isearch                       ((t (:foreground "#ffffff" :background "#ffbb00"))))
 `(isearch-lazy-highlight-face   ((t (:foreground "#ffffff" :background "#ff99bb"))))
 ;; (isearch-secondary           ((t (:foreground "green"))))
 `(menu                          ((t (:foreground "#ffffff" :background "#bbbbbb"))))
 `(minibuffer-prompt             ((t (:foreground "#555"))))
 `(mode-line                     ((t (:foreground "#444444" :background "#eeeeee" :box (:line-width 1 :color "#cccccc") :overline nil))))
 `(mode-line-inactive            ((t (:foreground "#888888" :background "#f4f4f4" :box (:line-width 1 :color "#dddddd") :overline nil))))
 `(mode-line-buffer-id           ((t (:foreground unspecified                     :weight bold))))
 `(mode-line-mousable            ((t (:foreground "#555" :background "#000"))))
 `(mode-line-mousable-minor-mode ((t (:foreground "#888" :background "#ffffff"))))
 `(region                        ((t (:background "#ffdd66"))))
 `(secondary-selection           ((t (:foreground "#0088ff" :background "#aaccff"))))
 `(show-paren-match-face         ((t (:foreground "#00bb00" :background "#bbffbb"))))
 `(show-paren-mismatch-face      ((t (:foreground "White" :background "Red"))))
 `(paren-face                    ((t (:foreground "#bbbbbb"))))
 `(tool-bar                      ((t (:foreground "#777" :background "#111"))))
 `(tooltip                       ((t (:foreground "#777" :background "#333"))))
 `(vertical-border               ((t (:foreground "#bbbbbb"))))
 `(widget-button-face            ((t (:foreground "#888" :weight bold))))
 `(widget-field-face             ((t (:foreground "#999" :weight bold)))))

(provide-theme 'whitestone)

;; Local Variables:
;; no-byte-compile: t
;; End:

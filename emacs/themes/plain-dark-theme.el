;; -*- lexical-binding: t; -*-

(deftheme plain-dark
  "A plain dark theme.")

(require 'lib-color)

(pcase-let*
    ((h.rand (random 360))
     (`(,c0 ,h0 ,h1 ,h2 ,h3 ,h4 ,h5)
      (random-element
       `(;; 0 0 54 ...
         ;; 0 0 120 ...
         (0 0 37 12 340 210 30)
         ;; yellow-banana: 70, yellow-saffron: 55.
         (0 0 100 60 140 210 30)
         (0 0 110 80 140 210 30)
         (0 0 195 270 160 210 30)
         (0 0 290 340 260 210 30)

         ;; A list with random hues:
         ;; (0 0 ,h.rand ,(mod (- h.rand 30) 360) ,(mod (+ h.rand 30) 360)
         ;;    ,(mod (+ h.rand 90) 360) 30)

         ;; Misc
         (0 0 220 100 340 210 30)
         )))

     (h.red 12)
     (h.green-grass 124)
     (h.green 127)
     (h.blue 240)
     (h.cyan 231)
     (h.purple 308)
     (h.yellow 86)
     (h.pink 350)

     (bg                       (color->hex 0 c0 h0))
     (fg                       (color->hex 76 0 0))
     (color.builtin            (color->hex 56 120 h1))
     (color.keyword            (color->hex 66 106 h2))
     (color.highlight          (color->hex 60 110 h4))
     (color.mode-line          (color->hex 60 0 0))
     (color.border             (color->hex 24 0 0)))

  (color-make-theme
   'plain-dark
   `((default                       ,fg ,bg)
     (cursor                        () (60 120 ,h1))
     ;; M-x list-faces-display
     (font-lock-bracket-face        (35 15 ,h1))
     (font-lock-builtin-face        ,color.builtin)
     (font-lock-comment-face        (45 ,c0 ,h3) () :italic t)
     (font-lock-comment-delimiter-face (25 ,c0 ,h3) () :italic t)
     (font-lock-constant-face       ,color.builtin)
     (font-lock-doc-face            ,color.builtin (10 ,c0 ,h0))
     (font-lock-function-name-face  ,fg () :weight bold)
     (font-lock-keyword-face        ,color.keyword)
     (font-lock-preprocessor-face   ,color.keyword)
     (font-lock-string-face         (53 ,c0 ,h0) (8 ,c0 ,h0))
     (font-lock-type-face           ,color.builtin)
     (font-lock-variable-name-face  ,fg () :weight bold)
     (font-lock-warning-face        (70 100 ,h5) () :weight bold)
     (dired-directory               (70 80 ,h.blue))
     (dired-symlink                 (70 80 ,h.purple) () :underline nil)
     (dired-ignored                 (53 0 0))
     (dired-marked                  (53 100 ,h.green) (8 40 ,h.green)
                                    :weight bold)
     (dired-flagged                 (64 160 ,h.red) (16 85 ,h.red)
                                    :weight bold)
     (dired-perm-write              ,color.builtin () :weight bold)
     (ediff-even-diff-A             () (8.2 0 0))
     (ediff-even-diff-B             () (8.2 0 0))
     (ediff-odd-diff-A              () (4.7 0 0))
     (ediff-odd-diff-B              () (4.7 0 0))
     (ediff-current-diff-A          () (16 25 12))
     (ediff-current-diff-B          () (16 25 127))
     (ediff-fine-diff-A             () (29 35 12))
     (ediff-fine-diff-B             () (29 35 127))
     (erc-notice-face               (30 0 0))
     (erc-nick-default-face         ,color.builtin)
     (ffap                          ,color.highlight)
     (flyspell-duplicate            () () :underline
                                    ,(list :style 'wave
                                           :color (color->hex 30 60 h5)))
     (flyspell-incorrect            () () :underline
                                    ,(list :style 'wave
                                           :color (color->hex 50 100 h5)))
     (fringe                        (9 ,c0 ,h0) ,bg)
     (header-line                   (38 ,c0 ,h0) (9 ,c0 ,h0))
     (highlight                     ,color.highlight (9 ,c0 ,h0))
     (holiday-face                  (43 ,c0 ,h0) ,fg)
     (isearch                       ,bg (80 120 ,h1))
     (lazy-highlight                ,bg (30 40 ,h1))
     (minibuffer-prompt             ,color.mode-line)
     (mode-line                     ,color.mode-line ,bg
                                    :box (:line-width 2 :color ,bg)
                                    :overline ,color.border)
     (mode-line-inactive            (30 0 0) ,bg
                                    :box (:line-width 2 :color ,bg)
                                    :overline ,color.border)
     (mode-line-highlight           ,color.highlight (10 30 ,h4))
     (mode-line-buffer-id           unspecified () :weight normal)
     (mode-line-mousable            (53 100 50) ,bg)
     (mode-line-mousable-minor-mode (38 ,c0 ,h0) ,bg)
     (outline-1                     (70 130 ,h1))
     (outline-2                     (60 100 ,h1))
     (outline-3                     (50 80 ,h1))
     (outline-4                     (40 60 ,h1))
     (org-document-info             (60 100 ,h2))
     (org-document-info-keyword     (30 40 ,h2))
     (org-document-title            (70 130 ,h2))
     (paren-face                    (35 15 ,h1))
     (region                        () (16 40 ,h1))
     (scroll-bar                    () (9 ,c0 ,h0))
     (show-paren-match              (70 80 ,h1) ,bg :underline t)
     (show-paren-mismatch           ,bg (50 150 ,h5) :weight bold)
     (tooltip                       (60 ,c0 ,h0) (10 ,c0 ,h0))
     (vertical-border               ,color.border)
     (widget-button-face            (38 ,c0 ,h0) () :weight bold)
     (widget-field-face             (32 ,c0 ,h0) () :weight bold))))

(provide-theme 'plain-dark)

;; Local Variables:
;; no-byte-compile: t
;; End:

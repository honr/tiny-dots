package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	homeDir        string
	gitDirPool     string
	workingDirPool string
)

func init() {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	gitDirPool = filepath.Join(homeDir, ".cache", "git")
	workingDirPool = filepath.Join(homeDir, ".opt", "src")
}

func die(msg string) {
	fmt.Fprintln(os.Stderr, msg)
	os.Exit(1)
}

func gitRemoteID(url string) (string, error) {
	switch {
	case strings.HasPrefix(url, "https://gitlab.com/"):
		return strings.TrimPrefix(url, "https://gitlab.com/"), nil
	case strings.HasPrefix(url, "https://github.com/"):
		return strings.TrimPrefix(url, "https://github.com/"), nil
	case strings.HasPrefix(url, "https://git.savannah.gnu.org/git"):
		remoteID := strings.TrimPrefix(url, "https://git.savannah.gnu.org/git/")
		remoteID = strings.TrimSuffix(remoteID, ".git")
		return remoteID, nil
	default:
		return "UNKNOWN", fmt.Errorf("unknown git remote URL: %s", url)
	}
}

func gitObtain(url string, localPath string, gitCloneArgs ...string) error {
	if url == "" {
		die("git-obtain: url local_path (the latter can be empty).")
	}

	remoteID, err := gitRemoteID(url)
	if err != nil {
		return err
	}

	gitDir := filepath.Join(gitDirPool, remoteID)
	workingDir := filepath.Join(workingDirPool, remoteID)

	if _, err := os.Stat(gitDir); !os.IsNotExist(err) {
		fmt.Println("Updating", remoteID)
		if _, err := os.Stat(workingDir); os.IsNotExist(err) {
			if err := os.MkdirAll(workingDir, 0755); err != nil {
				return err
			}
		}
		if err := os.Chdir(workingDir); err != nil {
			return err
		}

		cmd := exec.Command("git", "pull", "--ff-only", "origin", "master")
		cmd.Env = append(os.Environ(), fmt.Sprintf("GIT_DIR=%s", gitDir))
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}

		if err := os.Chdir(".."); err != nil {
			return err
		}

	} else {
		fmt.Println("Obtaining", remoteID, "from", url)
		gitDirParent := filepath.Dir(gitDir)
		workingDirParent := filepath.Dir(workingDir)
		if err := os.MkdirAll(gitDirParent, 0755); err != nil {
			return err
		}
		if err := os.MkdirAll(workingDirParent, 0755); err != nil {
			return err
		}

		args := []string{"clone", url, "--separate-git-dir=" + gitDir, workingDir}
		args = append(args, gitCloneArgs...)
		cmd := exec.Command("git", args...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
	}

	if localPath == "" {
		return nil
	}

	if _, err := os.Stat(localPath); os.IsNotExist(err) {
		localPathParent := filepath.Dir(localPath)
		if err := os.MkdirAll(localPathParent, 0755); err != nil {
			return err
		}
		if err := os.Symlink(workingDir, localPath); err != nil {
			return err
		}
	}

	return nil
}

func main() {
	if os.Getenv("TEST") == "" {
		args := os.Args[1:]
		if len(args) < 1 {
			die("usage: git obtain url local_path")
		}
		url := args[0]
		localPath := ""
		gitCloneArgs := []string{}
		if len(args) > 1 {
			localPath = args[1]
			if len(args) > 2 {
				gitCloneArgs = args[2:]
			}
		}
		if err := gitObtain(url, localPath, gitCloneArgs...); err != nil {
			die(err.Error())
		}
	}
}

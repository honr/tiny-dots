package main

import (
	"fmt"
	"io"
	"net"
	"os"

	"honr.gitlab.com/base/pasteboard"
)

var sockpath = "$HOME/.ssh/pasteboard.server"

func main() {
	sockpath = os.ExpandEnv(sockpath)
	os.Remove(sockpath)
	l, err := net.Listen("unix", sockpath)
	if err != nil {
		panic(err)
	}
	defer l.Close()
	fmt.Fprintf(os.Stderr, "Listening on sock: %s\n", sockpath)
	for {
		conn, err := l.Accept()
		if err != nil {
			panic(err)
		}
		go handle(conn)
	}
}

func handle(conn net.Conn) {
	defer conn.Close()
	data, err := io.ReadAll(conn)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	n, err := pasteboard.Write(data)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	fmt.Fprintf(os.Stderr, "Received %d bytes.\n", n)
}

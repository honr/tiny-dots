package topical

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
)

var (
	// TopicsDir is the directory where all the topics (as individual Org files)
	// are kept.
	TopicsDir = os.ExpandEnv("$HOME/Arc/tea")
	// ChromeUsersDir is where chrome user data directories are.  When running a
	// new chrome instance, a data dir can be selected using --user-data-dir.
	// Each user data dir can have multiple profile directories, accessible using
	// --profile-directory.
	ChromeUsersDir = os.ExpandEnv("$HOME/.config/chrome")

	configFile   = os.ExpandEnv("$HOME/Arc/.config")
	clDir        = os.ExpandEnv("$HOME/Arc/tea/cl")
	defaultMatch = os.ExpandEnv("$HOME")
)

type TopicInfo struct {
	Dir           string
	ChromeProfile string
}

// Get looks up topic in string table in file `configFile`.
func Get(topic string) (TopicInfo, error) {
	ti := TopicInfo{}
	if topic == "" {
		return ti, fmt.Errorf("Topic is empty")
	}
	// Temporarily set QUEST to let os.ExpandEnv expand it when reading the
	// configFile.
	oldDollarQUEST := os.Getenv("QUEST")
	os.Setenv("QUEST", topic)
	defer func() { os.Setenv("QUEST", oldDollarQUEST) }()

	f, err := os.Open(configFile)
	if err != nil {
		return ti, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			return ti, err
		}
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "#") { // Ignore comment lines.
			continue
		}
		lineChunks := strings.SplitN(line, " ", 3)
		if len(lineChunks) < 3 {
			continue
		}
		key := lineChunks[0]
		if !strings.HasPrefix(key, "/") || !strings.HasSuffix(key, "/") {
			// Plain string
			if key != topic {
				continue
			}
		} else {
			pattern := "^" + key[1:(len(key)-1)] + "$"
			if ok, err := regexp.MatchString(pattern, topic); err != nil || !ok {
				continue
			}
		}
		ti.Dir = os.ExpandEnv(lineChunks[1])
		ti.ChromeProfile = os.ExpandEnv(lineChunks[2])
		return ti, nil
	}
	return ti, fmt.Errorf("no matches found")
}

func readConfigFileKeys() ([]string, error) {
	l := []string{}
	f, err := os.Open(configFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			return nil, err
		}
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "#") { // Ignore comment lines.
			continue
		}
		lineChunks := strings.SplitN(line, " ", 3)
		if len(lineChunks) < 3 {
			continue
		}
		key := lineChunks[0]
		if strings.HasPrefix(key, "/") && strings.HasSuffix(key, "/") {
			continue
		}
		l = append(l, key)
	}
	return l, nil
}

func List() []string {
	m := map[string]int{}
	if configFileTopics, err := readConfigFileKeys(); err == nil {
		for _, x := range configFileTopics {
			m[x] = 1
		}
	}

	if files, err := ioutil.ReadDir(TopicsDir); err == nil {
		for _, f := range files {
			name := f.Name()
			if name == "cl" || strings.HasPrefix(name, ".") {
				continue
			}
			m[name] = 1
		}
	}
	if files, err := ioutil.ReadDir(ChromeUsersDir); err == nil {
		for _, f := range files {
			m[f.Name()] = 1
		}
	}
	if files, err := ioutil.ReadDir(clDir); err == nil {
		for _, f := range files {
			name := f.Name()
			if strings.HasPrefix(name, ".") {
				continue
			}
			name = strings.TrimSuffix(name, filepath.Ext(name))
			m[name] = 1
		}
	}
	l := make([]string, len(m))
	i := 0
	for k, _ := range m {
		l[i] = k
		i++
	}
	sort.Strings(l)
	return l
}

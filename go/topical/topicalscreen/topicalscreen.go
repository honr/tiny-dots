package topicalscreen

import (
	"os"
	"os/exec"

	"honr.gitlab.com/base/topical"
)

// ListSessions list current sessions.
func ListSessions() error {
	cmd := exec.Command("screen", "-list")
	cmd.Stdout = os.Stdout
	return cmd.Run()
}

// Attach creates a session, if needed, and attaches to it.  When set,
// `screenAttachArgs` list the `screen(1)` args used to resume the screen
// session.  The default is -dR (resume and detach other instances).  You can
// use -xR to attach as extra instances without detaching others.
func Attach(topic string, configDir string, screenAttachArgs []string) error {
	// Default value:
	if len(screenAttachArgs) == 0 {
		screenAttachArgs = []string{"-d", "-R"}
	}
	// In tmux, prefixing a session name with '=' makes it match the exact string.
	// Not sure how to do a similar enforcement in screen.
	cmd := exec.Command("screen", "-ls", topic)
	cmd.Stdin, cmd.Stderr, cmd.Stdout = nil, nil, nil
	if err := cmd.Run(); err != nil {
		// The session doesn't exist.  We need to make one.
		topicInfo, err := topical.Get(topic)
		if err != nil {
			return err
		}
		dir := topicInfo.Dir
		cmd = exec.Command("screen", "-d", "-m", "-S", topic)
		cmd.Dir = dir

		// TODO: Introduce a way to set Env vars for a particular session.  E.g.,
		// $STY.env
		// cmd.Env = ...
		cmd.Stderr, cmd.Stdout = os.Stderr, os.Stdout
		if err = cmd.Run(); err != nil {
			return err
		}
	}
	// TODO: source ~/T/$QUEST
	// topicInitFile := path.Join(configDir, topic) + ".rc"
	// if _, err := os.Stat(topicInitFile); err == nil {
	// 	cmd = exec.Command("sh", topicInitFile)
	// 	cmd.Stdin, cmd.Stderr, cmd.Stdout = os.Stdin, os.Stderr, os.Stdout
	// 	if err = cmd.Run(); err != nil {
	// 		return err
	// 	}
	// }
	cmd = exec.Command("screen", append(screenAttachArgs, topic)...)
	cmd.Stdin, cmd.Stderr, cmd.Stdout = os.Stdin, os.Stderr, os.Stdout
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

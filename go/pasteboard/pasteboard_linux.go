//go:build linux

package pasteboard

/*
#cgo LDFLAGS: -lc -lX11
#include <stdlib.h>

int pasteboardRead(void** buf);
int pasteboardWrite(char* buf, int size);
*/
import "C"
import (
	"errors"
	"unsafe"
)

func Read(p []byte) (int, error) {
	var data unsafe.Pointer
	n := int(C.pasteboardRead(&data))
 	if data == nil || n == 0 {
		return 0, errors.New("Failed to read pasteboard")
	}
	defer C.free(data)
	copy(p, C.GoBytes(data, C.int(n)))
	return n, nil
}

func Write(p []byte) (int, error) {
	n := len(p)
	if n == 0 {
		return 0, nil // Silently refuse to write an empty slice.
	}
	cBuf := C.CBytes(p)
	defer C.free(cBuf)
	m := int(C.pasteboardWrite((*C.char)(cBuf), C.int(n)))
	if m <= 0 {
		return 0, errors.New("Failed to write to pasteboard")
	}
	return m, nil
}

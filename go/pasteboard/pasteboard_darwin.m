//go:build darwin

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

int pasteboardRead(void **buf) {
	NSPasteboard* pasteboard = [NSPasteboard generalPasteboard];
	NSData* data = [pasteboard dataForType: NSPasteboardTypeString];
	if (data == nil) {
		return 0;
	}
	NSUInteger n = [data length];
	*buf = malloc(n);
	[data getBytes: *buf length: n];
	return n;
}

int pasteboardWrite(const void *buf, NSInteger n) {
	NSPasteboard* pasteboard = [NSPasteboard generalPasteboard];
	NSData* data = [NSData dataWithBytes: buf length: n];
	[pasteboard clearContents];
	if (![pasteboard setData: data forType:NSPasteboardTypeString]) {
		return -1;
	}
	return 0;
}

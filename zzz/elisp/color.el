(defun color--xyz-to-uv (x y z)
  (let ((denom (+ x (* 15.0 y) (* 3.0 z))))
    (if (zerop denom)
        (list 0.0 0.0)
      (list (/ (* 4.0 x) denom) (/ (* 9.0 y) denom)))))

(defun color--hex-to-rgb (c)
  "\"#0080ff\" -> (0 .502 1.0)."
  (list (/ (string-to-number (substring c 1 3) 16) 255.0)
        (/ (string-to-number (substring c 3 5) 16) 255.0)
        (/ (string-to-number (substring c 5 7) 16) 255.0)))

(defun color--rgb-to-hex (rgb)
  "(0 0.5 1.0) -> \"#0080ff\"."
  (cl-flet ((f (lambda (x)
                 (cond ((<= x 0) 0)
                       ((<= 1 x) 255)
                       (t (round (* 255.0 x)))))))
    (format "#%02X%02X%02X" (f (car rgb)) (f (cadr rgb)) (f (caddr rgb)))))

(defun color--rgb-to-xyz (RGB)
  (let ((rgb (mapcar (lambda (V)
                       ;; sRGB companding
                       (if (<= V 0.04045)
                           (/ V 12.92)
                         (expt (/ (+ V 0.055) (+ 1 0.055)) 2.4)))
                     RGB)))
    (mapcar (lambda (row) (apply #'+ (cl-mapcar '* row rgb)))
           (car (color--const 'sRGB)))))

(defun color--xyz-to-rgb (xyz)
  (mapcar (lambda (v)
            ;; sRGB companding
            (if (<= v 0.0031308)
                (* 12.92 v)
              (- (* (+ 1 0.055) (expt v (/ 1 2.4))) 0.055)))
          (mapcar (lambda (row) (apply #'+ (cl-mapcar '* row xyz)))
           (cadr (color--const 'sRGB)))))

(defun color--xyz-to-luv (xyz)
  (let* ((x (car xyz))
         (y (cadr xyz))
         (z (caddr xyz))

         (x_0 (car (color--const 'D65)))
         (y_0 (cadr (color--const 'D65)))
         (z_0 (caddr (color--const 'D65)))

         (uv_0 (color--xyz-to-uv x_0 y_0 z_0))
         (l (color--f1 (/ y y_0)))
         (uv_1 (color--xyz-to-uv x y z)))

    (list l
          (* 13.0 l (- (car uv_1) (car uv_0)))
          (* 13.0 l (- (cadr uv_1) (cadr uv_0))))))

(defun color--luv-to-xyz (luv)
  (let* ((l (car luv))
         (u (cadr luv))
         (v (caddr luv))

         (x_0 (car (color--const 'D65)))
         (y_0 (cadr (color--const 'D65)))
         (z_0 (caddr (color--const 'D65)))

         (uv_0 (color--xyz-to-uv x_0 y_0 z_0))
         (u_0 (car uv_0))
         (v_0 (cadr uv_0))

         (y (* y_0 (color--f1-inv l)))

         (a (* (/ 1.0 3)
               (if (< -0.0001 u 0.0001)
                   (/ (- 4.0 u_0) u_0)
                 (/ (- (* l (- 4.0 u_0))
                       (* (/ 1 13.0) u))
                    (+ (* l u_0)
                       (* (/ 1 13.0) u))))))

         (b (* -5.0 y))
         (c (/ -1 3.0))

         (d (* 5.0 y
               (if (< -0.0001 v 0.0001)
                   (/ (- (/ 3.0 5.0) v_0) v_0)
                 (/ (- (* l (- (/ 3.0 5.0) v_0))
                       (* (/ 1 13.0) v))
                    (+ (* l v_0)
                       (* (/ 1 13.0) v))))))
         (x (/ (- d b) (- a c)))
         (z (+ (* x a) b)))
    (list x y z)))

(defun color--luv-to-lch (luv)
  (let ((l (car luv))
        (u (cadr luv))
        (v (caddr luv)))
    (list l
          (sqrt (+ (* u u) (* v v)))
          (mod (* (/ 180 float-pi) (atan v u)) 360))))

(defun color--lch-to-luv (lch)
  (let ((l (car lch))
        (c (cadr lch))
        (h-radians (* (/ float-pi 180) (caddr lch))))
    (list l
          (* c (cos h-radians))
          (* c (sin h-radians)))))

;; For color selection, L*u*v (and its polar form, PolarLUV, aka HCL) is one of
;; the best color spaces.

(defun color-to-lch (c)
  (color--luv-to-lch
   (color--xyz-to-luv
    (color--rgb-to-xyz
     (color--hex-to-rgb c)))))

(defun color-from-lch (lch)
  (color--rgb-to-hex
   (color--xyz-to-rgb
    (color--luv-to-xyz
     (color--lch-to-luv lch)))))

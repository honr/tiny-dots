BEGIN {
  RS = ""
  FS = "\n"
  OFS = ""
  printf("[")
  delim = ""
}

{
  split($1, name, /, ?/)
  given_name = $2
  split($2, fnp, ": ")
  given_name = fnp[1]
  phone_number = fnp[2]
  printf(delim)
  printf("{\n")
  printf("  \"Name\": \"%s %s\",\n", given_name, name[1])
  printf("  \"Given Name\": \"%s\",\n", given_name)
  printf("  \"Last Name\": \"%s\",\n", name[1])
  printf("  \"Mobile Phone\": \"%s\",\n", phone_number)
  printf("  \"Categories\": \"Category\",\n")
  printf("  \"Notes\": \"%s\"\n", name[2])
  printf("}")
  delim = ", "
}

END {
  printf("]\n")
}
